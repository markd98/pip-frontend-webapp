// ** React Imports
import { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";

// ** Custom Components
import Avatar from "@components/avatar";

// ** Utils
import { isUserLoggedIn } from "@utils";

// ** Store & Actions
import { useDispatch } from "react-redux";
import { handleLogout } from "@store/actions/auth";

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Alert,
} from "reactstrap";

// ** Third Party Components
import {
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from "reactstrap";
import {
  User,
  Mail,
  CheckSquare,
  MessageSquare,
  Settings,
  CreditCard,
  HelpCircle,
  Power,
} from "react-feather";

// ** Default Avatar Image
import defaultAvatar from "@src/assets/images/portrait/small/avatar-s-11.jpg";

import { useSkin } from "@hooks/useSkin";
import { ChevronLeft } from "react-feather";
import { Redirect } from "react-router-dom";
import {
  Row,
  Col,
  CardTitle,
  CardText,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import "@styles/base/pages/page-auth.scss";
import OtpInput from "react-otp-input";
import InputPasswordToggle from "@components/input-password-toggle";
import OtpTimer from "otp-timer";
import validator from "validator";
import {
  ApiPost,
  ApiPostNoAuth,
  Bucket,
  ApiGetNoAuth,
  ApiGet,
} from "../../../../helpers/API/ApiData";
import { toast, ToastContainer, Slide } from "react-toastify";

const UserDropdown = () => {
  const [centeredModal, setCenteredModal] = useState(false);
  // ** Store Vars
  const dispatch = useDispatch();

  // ** State
  const [userData, setUserData] = useState(null);

  //** ComponentDidMount
  useEffect(async () => {
    if (isUserLoggedIn() !== null) {
      await ApiGet("").then((response) =>
        setUserData(
          response?.data?.data?.length > 0 && response?.data?.data?.[0]
        )
      );
      // setUserData(JSON.parse(localStorage.getItem("userData")));
    }
  }, []);

  //** Vars
  const userAvatar = (userData && userData.avatar) || defaultAvatar;

  const [skin, setSkin] = useSkin();
  const [changemodal, setchangemodal] = useState("1");
  const [npass, setnpass] = useState("");
  const [npassValidation, setnpassValidation] = useState("");
  const [cpass, setcpass] = useState("");
  const [cpassValidation, setcpassValidation] = useState("");
  const [email, setemail] = useState("");
  const [emailValidation, setemailValidation] = useState("");
  const [otp, setotp] = useState("");
  const [otpValidation, setotpValidation] = useState("");
  const [token, settoken] = useState({});

  const history = useHistory();

  const submitModal1 = async () => {
    if (email === "") {
      setemailValidation("Email is Requried");
    } else if (!validator.isEmail(email)) {
      setemailValidation("Enter Valid Email");
    } else {
      setemailValidation("");
      await ApiPostNoAuth("user/forgot_password", { email: email })
        .then((res) => {
          setchangemodal("2");
          toast.success(res.data.message);
        })
        .catch((err) => {
          toast.error(err.message);
        });
    }
  };

  const submitModal2 = async () => {
    if (otp === "") {
      setotpValidation("OTP is Requried");
    } else {
      setotpValidation("");
      await ApiPostNoAuth("user/otp_verification", { otp: otp })
        .then((res) => {
          settoken(res.data.data);
          setchangemodal("3");
          toast.success(res.data.message);
        })
        .catch((err) => {
          toast.error(err.message);
        });
    }
  };

  const submitModal3 = async () => {
    if (npass === "") {
      setnpassValidation("New Password is Requried");
    } else {
      setnpassValidation("");
    }
    if (cpass === "") {
      setcpassValidation("Confirm Password is Requried");
    } else {
      setcpassValidation("");
    }
    if (npass !== cpass) {
      setcpassValidation("Passwors is Not Match");
    } else {
      setcpassValidation("");
    }
    if (npass !== "" && cpass !== "" && npass === cpass) {
      await ApiPostNoAuth("user/reset_password", {
        id: token._id,
        authToken: token.authToken,
        password: npass,
      })
        .then((res) => {
          toast.success(res.data.message);
          localStorage.clear();
          history.push("/");
        })
        .catch((err) => {
          toast.error(err.message);
        });
    }
  };
  console.log("userData", userData);

  return (
    <>
      <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
        <DropdownToggle
          href="/"
          tag="a"
          className="nav-link dropdown-user-link"
          onClick={(e) => e.preventDefault()}
        >
          <Avatar
            img={
              userData?.image
                ? Bucket + userData?.image
                : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJxA5cTf-5dh5Eusm0puHbvAhOrCRPtckzjA&usqp=CAU"
            }
            imgHeight="40"
            imgWidth="40"
            status="online"
          />
          <div className="user-nav d-sm-flex d-none ml-1">
            <span className="user-name font-weight-bold">
              {userData && userData["username"]}
            </span>
            {/* <span className='user-status'>{(userData && userData.role) || 'Admin'}</span> */}
          </div>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem tag={Link} to="/pages/profile">
            <User size={14} className="mr-75" />
            <span className="align-middle">Profile</span>
          </DropdownItem>
          <DropdownItem
            tag={Link}
            onClick={() => setCenteredModal(!centeredModal)}
          >
            <User size={14} className="mr-75" />
            <span className="align-middle">Change Password</span>
          </DropdownItem>
          {/* <DropdownItem tag={Link} to='/apps/email'>
          <Mail size={14} className='mr-75' />
          <span className='align-middle'>Inbox</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/apps/todo'>
          <CheckSquare size={14} className='mr-75' />
          <span className='align-middle'>Tasks</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/apps/chat'>
          <MessageSquare size={14} className='mr-75' />
          <span className='align-middle'>Chats</span>
        </DropdownItem>
        <DropdownItem divider />
        <DropdownItem tag={Link} to='/pages/account-settings'>
          <Settings size={14} className='mr-75' />
          <span className='align-middle'>Settings</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/pages/pricing'>
          <CreditCard size={14} className='mr-75' />
          <span className='align-middle'>Pricing</span>
        </DropdownItem>
        <DropdownItem tag={Link} to='/pages/faq'>
          <HelpCircle size={14} className='mr-75' />
          <span className='align-middle'>FAQ</span>
        </DropdownItem> */}
          <DropdownItem
            tag={Link}
            to="/login"
            onClick={() => dispatch(handleLogout())}
          >
            <Power size={14} className="mr-75" />
            <span className="align-middle">Logout</span>
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
      <Modal
        isOpen={centeredModal}
        toggle={() => setCenteredModal(!centeredModal)}
        className="modal-dialog-centered"
      >
        <ModalHeader toggle={() => setCenteredModal(!centeredModal)}>
          Change Password
        </ModalHeader>
        <ModalBody>
          {changemodal === "1" && (
            <Col
              className="d-flex align-items-center auth-bg px-2 p-lg-2"
              lg="12"
              sm="12"
            >
              <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
                <CardText className="mb-2">
                  Enter your email to get the otp
                </CardText>
                <Form
                  className="auth-forgot-password-form mt-2"
                  onSubmit={(e) => e.preventDefault()}
                >
                  <FormGroup>
                    <Label className="form-label" for="login-email">
                      Email
                    </Label>
                    <Input
                      type="email"
                      id="login-email"
                      placeholder="john@example.com"
                      autoFocus
                      value={email}
                      onChange={(e) => setemail(e.target.value)}
                    />
                    {emailValidation && (
                      <div className="validation">{emailValidation}</div>
                    )}
                  </FormGroup>
                  <Button.Ripple
                    color="primary"
                    block
                    onClick={() => submitModal1()}
                    className="common_button"
                  >
                    Submit
                  </Button.Ripple>
                </Form>
              </Col>
            </Col>
          )}
          {changemodal === "2" && (
            <Col
              className="d-flex align-items-center auth-bg px-2 p-lg-2"
              lg="12"
              sm="12"
            >
              <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
                <CardText className="mb-2">
                  Enter the otp sent to your given email
                </CardText>
                <Form
                  className="auth-forgot-password-form mt-2"
                  onSubmit={(e) => e.preventDefault()}
                >
                  <FormGroup>
                    <Label className="form-label" for="login-email">
                      OTP
                    </Label>
                    <div className="d-flex ">
                      <OtpInput
                        value={otp}
                        onChange={(e) => setotp(e)}
                        numInputs={6}
                        separator={<span className="mr-10"> </span>}
                        isInputNum
                        inputStyle={{
                          width: "45px",
                          height: "45px",
                          marginRight: "7px",
                        }}
                        containerStyle={{ width: "100%" }}
                      />
                      <br />
                      <br />
                    </div>
                    {otpValidation && (
                      <div className="validation">{otpValidation}</div>
                    )}
                  </FormGroup>
                  <Button.Ripple
                    color="primary"
                    block
                    onClick={() => submitModal2()}
                    className="common_button"
                  >
                    Submit
                  </Button.Ripple>
                </Form>
                <p className="text-center mt-2 d-flex justify-content-between">
                  <div className="otp">
                    <OtpTimer
                      seconds={59}
                      minutes={1}
                      resend={() => {
                        submitModal1();
                      }}
                      text="Resend OTP in"
                      ButtonText="Resend OTP"
                      textColor="#7367f0"
                      buttonColor={"#7367f0"}
                      background={"#fff"}
                    />
                  </div>
                  {/* <Link to=''>
                    <small>Resend OTP</small>
                  </Link> */}
                </p>
              </Col>
            </Col>
          )}
          {changemodal === "3" && (
            <Col
              className="d-flex align-items-center auth-bg px-2 p-lg-2"
              lg="12"
              sm="12"
            >
              <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
                <Form
                  className="auth-forgot-password-form mt-2"
                  onSubmit={(e) => e.preventDefault()}
                >
                  <FormGroup>
                    <div className="d-flex justify-content-between">
                      <Label className="form-label" for="login-password">
                        New Password
                      </Label>
                    </div>
                    <InputPasswordToggle
                      value={npass}
                      id="login-password"
                      name="login-password"
                      className="input-group-merge"
                      placeholder="123"
                      onChange={(e) => setnpass(e.target.value)}
                      // className={classnames({ 'is-invalid': errors['login-password'] })}
                      // innerRef={register({ required: true, validate: value => value !== '' })}
                    />
                    {npassValidation && (
                      <div className="validation">{npassValidation}</div>
                    )}
                  </FormGroup>
                  <FormGroup>
                    <div className="d-flex justify-content-between">
                      <Label className="form-label" for="login-password">
                        Confirm Password
                      </Label>
                    </div>
                    <InputPasswordToggle
                      value={cpass}
                      placeholder="123"
                      id="login-password"
                      name="login-password"
                      className="input-group-merge"
                      onChange={(e) => setcpass(e.target.value)}
                      // className={classnames({ 'is-invalid': errors['login-password'] })}
                      // innerRef={register({ required: true, validate: value => value !== '' })}
                    />
                    {cpassValidation && (
                      <div className="validation">{cpassValidation}</div>
                    )}
                  </FormGroup>
                  <Button.Ripple
                    color="primary"
                    block
                    onClick={() => submitModal3()}
                    className="common_button"
                  >
                    Update Password
                  </Button.Ripple>
                </Form>
              </Col>
            </Col>
          )}
        </ModalBody>
      </Modal>
    </>
  );
};

export default UserDropdown;
