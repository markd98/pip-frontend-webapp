import {
  Box,
  Mail,
  MessageSquare,
  CheckSquare,
  Calendar,
  FileText,
  Circle,
  ShoppingCart,
  User,
} from "react-feather";

export default [
  {
    id: "apps",
    title: "Apps",
    icon: <Box />,
    children: [
      {
        id: "new_idea",
        title: "New Idea",
        icon: <Mail size={20} />,
        navLink: "/apps/newIdea",
      },
      {
        id: "summary",
        title: "PIP Summary",
        icon: <MessageSquare size={20} />,
        navLink: "/apps/summary",
      },
      {
        id: "ranking",
        title: "PIP Ranking",
        icon: <CheckSquare size={20} />,
        navLink: "/apps/ranking",
      },
      {
        id: "services",
        title: "PIP Services",
        icon: <Calendar size={20} />,
        navLink: "/apps/services",
      },
      {
        id: "message",
        title: "Message Center",
        icon: <FileText size={20} />,
        navLink: "/apps/message",
      },
      {
        id: "withdrawal",
        title: "Withdrawal",
        icon: <ShoppingCart size={20} />,
        navLink: "/apps/withdrawal",
      },
      {
        id: "blockChain",
        title: "Block chain",
        icon: <User size={20} />,
        navLink: "/apps/blockChain",
      },
    ],
  },
];
