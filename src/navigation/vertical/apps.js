import {
  Mail,
  MessageSquare,
  CheckSquare,
  Calendar,
  FileText,
  Circle,
  ShoppingCart,
  User,
  List,
  DollarSign,
  Server
} from "react-feather";

export default [
  {
    id: "new_idea",
    title: "New Idea",
    icon: <Mail size={20} />,
    navLink: "/apps/newIdea",
  },
  {
    id: "new_idea",
    title: "My Idea",
    icon: <Server  size={20} />,
    navLink: "/apps/myIdea",
  },
  {
    id: "support",
    title: "PIP Support",
    icon: <MessageSquare size={20} />,
    navLink: "/apps/support",
  },
  {
    id: "summary",
    title: "PIP Summary",
    icon: <List size={20} />,
    navLink: "/apps/summary",
  },
  {
    id: "ranking",
    title: "PIP Ranking",
    icon: <CheckSquare size={20} />,
    navLink: "/apps/ranking",
  },
  {
    id: "services",
    title: "PIP Services",
    icon: <Calendar size={20} />,
    navLink: "/apps/services",
  },
  {
    id: "message",
    title: "Message Center",
    icon: <FileText size={20} />,
    navLink: "/apps/message",
  },
  // {
  //   id: "withdrawal",
  //   title: "Withdrawal",
  //   icon: <ShoppingCart size={20} />,
  //   navLink: "/apps/withdrawal",
  // },
  {
    id: "byupip",
    title: "Buy PIP Tokens",
    icon: <DollarSign size={20} />,
    navLink: "/apps/buypip",
  },
  {
    id: "blockChain",
    title: "Blockchain",
    icon: <User size={20} />,
    navLink: "/apps/blockchain",
  },
];
