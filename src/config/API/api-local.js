const protocol = "https";
const host = "development.api.unicornui.com";
const port = "";
const trailUrl = "";
const hostUrl = `http://api.pipidea.com/`;
const endpoint = `http://api.pipidea.com`;

// const hostUrl = `http://localhost/`;
// const endpoint = `http://localhost`;

const config = {
  protocol: protocol,
  host: host,
  port: port,
  apiUrl: trailUrl,
  endpoint: endpoint,
  hostUrl: hostUrl,
};
export default config;
