import React from 'react'
import { selectThemeColors } from '@utils'
import Select from 'react-select'
import {
    Button,
    Card,
    CardBody,
    CardTitle,
    Col,
    CustomInput,
    FormGroup,
    Input,
    CardText,
    Label,
    Row,
  } from "reactstrap";
import { Link, useHistory } from 'react-router-dom'


  const data = [
    {
      id: 1,
      name: "Idea 1",
      pip: "Shared",
      price : "P2000",
      status  :"Pat Pend"
    },
    {
      id: 2,
      name: "Idea 2",
      price : "0",
      pip: "Shared",
      status  :"No Pat"
    },
    {
      id: 3,
      name: "Idea 3",
      pip: "Shared",
      price : "P10000",
      status  :"Patented"

    },
   
  ];

const colourOptions = [
  { value: 'India', label: 'India' },
  { value: 'USA', label: 'USA' },
  { value: 'Canada', label: 'Canada' }
]

const PipIdea = () => {
    const history = useHistory()
    return (
        <><Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">PIP Idea</CardTitle>
          <hr />
          <Row>
          <Col xl="12">
          <FormGroup>
                      <Label className="text-uppercase letter-spacing" for='register-username' >
                      sort by
                      </Label>
                      <Select
                    theme={selectThemeColors}
                    className='react-select'
                    classNamePrefix='select'
                    defaultValue={colourOptions[1]}
                    name='clear'
                    options={colourOptions}
                    isClearable
                  />
                    </FormGroup>
          </Col>

         <div className='mt-2 w-100'>
         {data.map((item, i) => (
            <Col xl="12">
              <FormGroup className=" row align-items-center mx-0 ">
                
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0 w-25 text-left cursor-pointer" >{item.name}</p>
                  <p className="m-0 w-25 text-center">{item.pip}</p>
                  <p className="m-0 w-25 text-center">{item.price}</p>
                  <p className="m-0 w-25 text-right cursor-pointer" >{item.status}</p>
                </div>
              </FormGroup>
             
            </Col>
            
          ))}

         </div>
         <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={()=> history.push("/apps/edit-pip")}>Edit PIP Idea</Button>
          </Col>
          </Row>
          </CardBody>
          </Card>
          </>
    )
}

export default PipIdea
