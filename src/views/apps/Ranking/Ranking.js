import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { Link, useHistory } from 'react-router-dom'
import sort from "../../../assets/images/myImg/sort.png"
const data = [
  {
    id: 1,
    name: "User A",
    pip: "21 Pips",
    price : "P258300",
  },
  {
    id: 2,
    name: "User B",
    price : "P234000",
    pip: "20 Pips",
  },
  {
    id: 3,
    name: "User C",
    pip: "18 Pips",
    price : "P201000",

  },
  {
    id: 4,
    name: "User D",
    pip: "15 Pips",
    price : "P180020",

  }, {
    id: 5,
    name: "User E",
    pip: "14 Pips",
    price : "P173899",

  },
 
];

const Ranking = () => {
  const history = useHistory()
  return <><Card className=" w-100 scroller">
  <CardBody>
    <CardTitle tag="h2">PIP Rankings</CardTitle>
    <hr />
    <Row>
    
    <Col xl="12" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <b className="m-0"><img src={sort}/></b>
                  <b className="m-0"><img src={sort}/></b>
                  <b className="m-0"><img src={sort}/></b>
                </div>
              </FormGroup>
             
            </Col> 
            {data.map((item, i) => (
            <Col xl="12">
              <FormGroup className=" row align-items-center mx-0 ">
                
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 cursor-pointer" onClick={()=> history.push("/apps/pip-idea")}>
                  <p className="m-0 w-33 text-left" >{item.name}</p>
                  <p className="m-0 w-33 text-center">{item.pip}</p>
                  <p className="m-0 w-33 text-right">{item.price}</p>
                </div>
              </FormGroup>
             
            </Col>
            
          ))}
            <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={() => history.push("/apps/newIdea")}>Home</Button>
          </Col>
            </Row>
            </CardBody>
            </Card>
            </>
};

export default Ranking;
