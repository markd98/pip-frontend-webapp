import React, {useState, useEffect} from 'react'

import { selectThemeColors } from '@utils'
import Select from 'react-select'
import {
    Button,
    Card,
    CardBody,
    CardTitle,
    Col,
    CustomInput,
    FormGroup,
    Input,
    CardText,
    Label,
    Row,
  } from "reactstrap";
import { Link, useHistory } from 'react-router-dom'
import mark from "../../../assets/images/myImg/mark.png"
import queryString from'query-string';
import { ApiPost, ApiPostNoAuth, Bucket,ApiGet,ApiUpload } from "../../../helpers/API/ApiData";

const colourOptions = [
    { value: 'India', label: 'India' },
    { value: 'USA', label: 'USA' },
    { value: 'Canada', label: 'Canada' }
  ]
  
  const data = [
      {
          name : "PIP Commit",
          status : 1,
      },
      {
        name : "Patent Request",
        status : 0,
    }, {
        name : "App. Confirmed",
        status : 0,
    }, {
        name : "Patent Comm",
        status : 1,
    }, {
        name : "Patent Pending",
        status : 0,
    }, {
        name : "Patent Issues",
        status : 1,
    }, {
        name : "Patent Update",
        status : 0,
    }, {
        name : "Patent Issued",
        status : 0,
    },
  ]
const PipPatentStatus = () => {
  const [status, setstatus] = useState({})
    const history = useHistory()
    const parsed = queryString.parse(location.search);

    
  const getStatus =async () => {
    await ApiGet(`/get_idea_status_by_id/${parsed.id}`)
    .then((res) => {
      setstatus(res?.data?.data)
    })
    .catch((err) => {});

  }
  useEffect(async() => {
   await getStatus()
  }, [])

    return (
        <><Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">PiP Patent Status</CardTitle>
         <hr />
          <Row>
          <Col xl="12" className="mb-2">
          <FormGroup>
                      <Label className="text-uppercase letter-spacing" for='register-username' >
                      pip patent application status
                      </Label>
                      {/* <Select
                    theme={selectThemeColors}
                    className='react-select'
                    classNamePrefix='select'
                    defaultValue={colourOptions[1]}
                    name='clear'
                    options={colourOptions}
                    isClearable
                  /> */}
                    <Input
                type="name"
                id="basicInput"
                placeholder="My First Great PIP Idea"
                name="name"
                value={parsed.name}
                // onChange={handleChange}
                readonly="readonly"
              />
                    </FormGroup>
          </Col>
       
         {Object.entries(status)?.map(v => {
              return <>
               <Col xl="3" sm="12" md="3"  >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className={`border rounded row justify-content-between m-0 px-1 py-2 flex-grow-1 cursor-pointer ${v[1]&& "bg-gray"}`} >
                  <p className="m-0">{v[0]}</p>
                 
                </div>
              {v[1]  &&  <img src={mark} className='mark'></img>}
              </FormGroup>
              
             
            </Col> 
              </>
          })}
       
         
          <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={()=> history.push("/apps/newIdea")}>Home</Button>
          </Col>
          </Row>
          </CardBody>
          </Card>
          </>
    )
}

export default PipPatentStatus
