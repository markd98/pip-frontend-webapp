import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  CustomInput,
  FormGroup,
  Input,
  Label,
  Row,
  CardTitle,
} from "reactstrap";
import Uppy from "@uppy/core";
import thumbnailGenerator from "@uppy/thumbnail-generator";
import { DragDrop } from "@uppy/react";
import Select, { components } from "react-select";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToMarkdown from "draftjs-to-markdown";
import { convertToRaw } from "draft-js";
import "uppy/dist/uppy.css";
import "@uppy/status-bar/dist/style.css";
import "@styles/react/libs/file-uploader/file-uploader.scss";
import "@styles/react/libs/editor/editor.scss";
import { NavLink, useHistory } from "react-router-dom";

import {
  ApiPost,
  ApiPostNoAuth,
  Bucket,
  ApiGet,
  ApiUpload,
} from "../../../helpers/API/ApiData";
import RichTextEditor from "react-rte";
import Dropzone from "react-dropzone";
import { ToastContainer, toast } from "react-toastify";
import { Modal, ModalHeader, ModalBody, ModalFooter, Alert } from "reactstrap";

// import CancelIcon from "@material-ui/icons/Cancel";

// const colorOptions = [
//   { value: "ocean", label: "Ocean", color: "#00B8D9", isFixed: true },
//   { value: "blue", label: "Blue", color: "#0052CC", isFixed: true },
//   { value: "purple", label: "Purple", color: "#5243AA", isFixed: true },
//   { value: "red", label: "Red", color: "#FF5630", isFixed: false },
//   { value: "orange", label: "Orange", color: "#FF8B00", isFixed: false },
//   { value: "yellow", label: "Yellow", color: "#FFC400", isFixed: false },
// ];

const New_Idea = () => {
  const value1 = "<p>Test <strong>editor</strong> with simple text 😀</p>";
  const [img, setImg] = useState(null);
  const [value, setValue] = useState(EditorState.createEmpty());
  const [ipcList, setipcList] = useState([]);
  const [companyList, setcompanyList] = useState([]);
  const [pipData, setpipData] = useState({});
  const [unPurchasedService, setunPurchasedService] = useState([]);
  const [purchasedServices, setpurchasedService] = useState([]);
  const [errors, setError] = useState({});
  const [richValue, setrichValue] = useState(RichTextEditor.createEmptyValue());
  // const [richValue, setrichValue] = useState(RichTextEditor.createValueFromString(value1, 'markdown'))
  const [setimage, setImage] = useState([]);
  const [funding, setfunding] = useState({});
  const [ImageArray, setImageArray] = useState([]);

  useEffect(() => {
    if (localStorage.getItem("funding"))
      setfunding(JSON.parse(localStorage.getItem("funding")));
  }, []);

  

  const history = useHistory();
  let setimagearray = [];

  const getServices = async () => {
    await ApiGet("/service")
      .then((res) => {
        setunPurchasedService(res?.data?.data?.unPurchasedService);
        setpurchasedService(res?.data?.data?.purchasedService);
        // setpipData({
        //   ...pipData,
        //   pipServiceId: res?.data?.data?.purchasedService?.map((v) => v._id),
        // });
      })
      .catch((err) => {});
  };
  useEffect(async () => {
    await getServices();
    if (localStorage.getItem("idea")) {
      setpipData(JSON.parse(localStorage.getItem("idea")));
      let data = JSON.parse(localStorage.getItem("idea"));

      // setrichValue(data?.description)
      if (data?.description)
        setrichValue(
          RichTextEditor?.createValueFromString(
            data?.description?.toString()?.replace(/<[^>]+>/g, ""),
            "markdown"
          )
        );
    }
    if (localStorage.getItem("image")) {
      setImageArray(JSON.parse(localStorage.getItem("imagearray")));
      setImage(JSON.parse(localStorage.getItem("image")));
    }
  }, []);

  const handleChange = (e) => {

    const { name, value } = e.target;
    setpipData({
      ...pipData,
      [name]: value,
    });
  };

  const getIpc = async () => {
    await ApiGet("/ipc")
      .then((res) => {
        setipcList(res?.data?.data);
      })
      .catch((err) => {});

    await ApiGet("/company")
      .then((res) => {
        setcompanyList(res?.data?.data);
      })
      .catch((err) => {});
  };
  useEffect(async () => {
    await getIpc();
    localStorage.setItem("isValidate", false);
  }, []);

  useEffect(() => {
    // if(localStorage.getItem("idea")){
    //   let d = JSON.parse(localStorage.getItem("idea"))
    //   setpipData(d)
    // }
  }, []);

  let colorOptions = ipcList?.map((v) => {
    return { value: v._id, label: v.name };
  });
  let colorOptions1 = companyList?.map((v) => {
    return { value: v._id, label: v.name };
  });

  const onChange = (value) => {
    // this.setState({value});
    setrichValue(value);
    // if (this.props.onChange) {
    // Send the changes up to the parent component as an HTML string.
    // This is here to demonstrate using `.toString()` but in a real app it
    // would be better to avoid generating a string on each change.
    // this.props.onChange(
    value.toString("html");
    setpipData({ ...pipData, description: value.toString("html") });

    // );
  };

  const uppy = new Uppy({
    meta: { type: "avatar" },
    restrictions: { maxNumberOfFiles: 1 },
    autoProceed: true,
  });

  uppy.use(thumbnailGenerator);

  uppy.on("thumbnail:generated", (file, preview) => {
    setImg(preview);
  });

  const handleDesc = (data) => {
    setValue(data);
    let data1 = draftToMarkdown(convertToRaw(data.getCurrentContent()));
    let sss = data1.replace("&nbsp;", " ");
    let sss1 = sss.replace("\n", " ");
    setpipData({ ...pipData, description: sss1 });
  };

  const onDrop3 = (files1) => {
    files1.map((file) => {
      let fileURL = URL.createObjectURL(file);

      file.fileURL = fileURL;
      setImage([file]);
    });


    // let file = e.target.files[0];
    // let fileURL = URL.createObjectURL(file);
    // file.fileURL = fileURL;
    // // }
    // setImage([file]);
  };

  const deleteimage3 = (value, index) => {
    let delteteImg = setimage.filter((x, i) => i != index);
    setImage(delteteImg);
  };

  const validateForm = () => {
    let errors = {};
    let formIsValid = true;

    if (!pipData.name) {
      formIsValid = false;
      errors["name"] = " Name is requried";
    }

    // if (
    //   pipData.description == "" ||
    //   pipData.description == "<p><br></p>" ||
    //   !pipData.description
    // ) {
    //   formIsValid = false;
    //   errors["description"] = "Description is requried";
    // }
    // if (!pipData.companyId) {
    //   formIsValid = false;
    //   errors["companyId"] = "Company is Requried";
    // }
    // if (!pipData.ipcId) {
    //   formIsValid = false;
    //   errors["ipcId"] = "Ipc is Requried";
    // }
    // if (!pipData.salesPrice) {
    //   formIsValid = false;
    //   errors["salesPrice"] = "Sales Price is Requried";
    // }
    // if (!pipData.visibility) {
    //   formIsValid = false;
    //   errors["visibility"] = "visibility is Requried";
    // }
    // if (setimage?.length == 0) {
    //   formIsValid = false;
    //   errors["image"] = "*Please Enter Images";
    // }

    // if (purchasedServices.length == 0) {
    //   formIsValid = false;
    //   errors["pipServiceId"] = "PIP Service is Requried";
    // }
    // if (!funding?.MembershipName) {
    //   formIsValid = false;
    //   errors["idea"] = "Idea Funding is Requried";
    // }

    setError(errors);
    return formIsValid;
  };

  const imagearrayapi = async () => {
    let image = [];

    for (let i = 0; i < setimage.length; i++) {

      if (setimage[i].fileURL) {
        const formData = new FormData();
        formData.append("image", setimage[i]);


        if (formData) {
          await ApiUpload("upload/compress_image/profile", formData)
            .then((res) => {
              image.push(res.data.data.image);
            })
            .catch((err) => {
              if (err.status == 410) {
                // refreshtoken();
                imagearrayapi();
              } else {
                // toast.error(err.message);
              }
            });
        }
      } else {
        image.push(setimage[i]);
      }
    }
    return image;
  };

  const saveIdea = async () => {
    if (validateForm()) {
      let body;
      if (ImageArray.length <= 0) {
        setimagearray = await imagearrayapi();
        body = {
          ...pipData,
          //  ...funding,
          document: setimagearray,
          pipServiceId: purchasedServices.map((v) => v._id),
        };
      } else {
        body = {
          ...pipData,
          //  ...funding,
          document: ImageArray,
          pipServiceId: purchasedServices.map((v) => v._id),
        };
      }



      await ApiPost("/idea", body)
        .then((res) => {
          localStorage.removeItem("funding");
          localStorage.removeItem("image");
          localStorage.removeItem("idea");
          localStorage.removeItem("imagearray");
          history.push(
            `/apps/pip-idea-membership?id=${res.data?.data?._id}&name=${res.data?.data?.name}`
          );

          // setCenteredModal3(!centeredModal3)
          toast.success(res.data.message);
        })
        .catch((err) => {
          toast.error(err.message);
        });
    }
  };

  const openFunding = async () => {
    localStorage.setItem("idea", JSON.stringify(pipData));
    localStorage.setItem("image", JSON.stringify(setimage));
    let setimagearray = await imagearrayapi();
    localStorage.setItem("imagearray", JSON.stringify(setimagearray));

    history.push("/apps/pip-idea-membership");
  };


  let selected = companyList?.find((v) => v._id === pipData?.companyId);
  let selected2 = ipcList?.find((v) => v._id === pipData?.ipcId);


  const serr =async () => {
    localStorage.setItem("idea", JSON.stringify(pipData));
    localStorage.setItem("image", JSON.stringify(setimage));
    let setimagearray = await imagearrayapi();
    localStorage.setItem("imagearray", JSON.stringify(setimagearray));

    history.push("/apps/services")
  }
  return (
    <>
    <ToastContainer/>
      <Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">New idea</CardTitle>
          <hr />
          <Row>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  name your pip
                </Label>
                <Input
                  type="name"
                  id="basicInput"
                  placeholder="My First Great PIP Idea"
                  name="name"
                  value={pipData.name}
                  onChange={handleChange}
                />
                <div className="validation">{errors?.name}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  Explain your new idea
                </Label>
                {/* <Editor
                editorState={value}
                onEditorStateChange={(data) =>handleDesc(data) }

              /> */}
                <RichTextEditor value={richValue} onChange={onChange} />
                <div className="validation">{errors?.description}</div>
              </FormGroup>
            </Col>
            <Col sm="12" className="mt-2">
              <FormGroup>
                {/* <DragDrop uppy={uppy} />
              {img !== null ? (
                <img className="rounded mt-2" src={img} alt="avatar" />
              ) : null} */}
                <Dropzone onDrop={onDrop3} accept={"image/*"} multiple={false}>
                  {({ getRootProps, getInputProps }) => (
                    <div className="dropZone" {...getRootProps()}>
                      <input {...getInputProps()} />
                      <p>
                        Drag 'n' drop some files here, or click to select files
                      </p>
                      {/* <img
                        src="images/upload-images.png"
                        alt="logo"
                        className="desktop-logo"
                        style={{ width: "40%" }}
                      ></img> */}
                    </div>
                  )}
                </Dropzone>
                <div className="validation">{errors?.image}</div>
                {setimage.length > 0 && (
                  <div style={{ height: " 160px" }}>
                    {setimage.map((files, i) => (
                      <Col
                        key={i}
                        md={2}
                        style={{
                          paddingTop: "20px",
                        }}
                      >
                        <img
                          src={files.fileURL ? files.fileURL : Bucket + files}
                          className="uploadedImage"
                          style={{ height: "150px" }}
                        />
                        {/* <CancelIcon
                        onClick={() => deleteimage3(files, i)}
                        className="imagecancelButton"
                      ></CancelIcon> */}
                      </Col>
                    ))}
                  </div>
                )}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  Select Company if applicable
                </Label>
                <Select
                  isClearable={false}
                  //   theme={selectThemeColors}
                  //   defaultValue={colorOptions[1]}
                  options={colorOptions1}
                  //   formatGroupLabel={formatGroupLabel}
                  className="react-select"
                  classNamePrefix="select"
                  name="companyId"
                  value={colorOptions1.filter(
                    (option) => option.label === selected?.name
                  )}
                  // value={pipData.companyId}
                  onChange={(e) =>
                    setpipData({ ...pipData, companyId: e.value })
                  }
                />
                <div className="validation">{errors?.companyId}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  select IPC
                </Label>
                <Select
                  isClearable={false}
                  //   theme={selectThemeColors}
                  //   defaultValue={colorOptions[1]}
                  options={colorOptions}
                  //   formatGroupLabel={formatGroupLabel}
                  className="react-select"
                  classNamePrefix="select"
                  name="ipcId"
                  value={colorOptions.filter(
                    (option) => option.label === selected2?.name
                  )}
                  // value={pipData.ipcId}
                  onChange={(e) => setpipData({ ...pipData, ipcId: e.value })}
                />
                <div className="validation">{errors?.ipcId}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup className="text-center">
                <p
                
                  className="text-uppercase letter-spacing blue cursor-pointer"
                  onClick={serr}
                >
                  Click for PIP Services
                </p>
                <div className="validation">{errors?.pipServiceId}</div>
              </FormGroup>
              {purchasedServices.length > 0 && (
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  Purchased Services
                </Label>
              )}
              <Row>
                {purchasedServices?.map((item, i) => (
                  <Col xl="12">
                    <FormGroup className=" row align-items-center mx-0 ">
                      {/* <CustomInput
                  type="checkbox"
                  id={i}
                  name="customRadio"
                  inline
                  label=""
                  onChange={(e)=> checkedValue(e,item)}
                  // defaultChecked
                /> */}
                      <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                        <p className="m-0">{item.name}</p>
                        <b className="m-0">{item.price} PIP</b>
                      </div>
                    </FormGroup>
                  </Col>
                ))}
                {/* <div className="d-flex justify-content-between w-100 card-body " style={{paddingLeft : "5%"}}> 
               <h5 className="fw-600">Total Services </h5> 
               <h5 className="blue fw-600"> 1500 Pips</h5>
             </div>

          <Col xl="12" className="my-2">
            <Button className="common_button" onClick={()=> payment()}>Payment</Button>
          </Col> */}
              </Row>
            </Col>

            {/* <Col xl="12" className="mt-2">
              <FormGroup>
                <div
                  className="border rounded row justify-content-between m-0 px-1 py-50 cursor-pointer"
                  onClick={() => openFunding()}
                >
                  <p className="m-0">Idea Funding</p>
                </div>
                <div className="validation">{errors?.idea}</div>
              </FormGroup>
            </Col> */}
            <Col xl="12" className="mt-2">
              <FormGroup>
                {/* <div className="border rounded row justify-content-between m-0 px-1 py-50"> */}
                {/* <p className="m-0">Current Sales Price</p> */}
                {/* <b className="m-0">$10000</b> */}
                <Input
                  type="text"
                  id="basicInput"
                  placeholder="Current Sales Price"
                  name="salesPrice"
                  value={pipData.salesPrice}
                  onChange={handleChange}
                />
                <div className="validation">{errors?.salesPrice}</div>
                {/* </div> */}
              </FormGroup>
            </Col>

            <Col xl="12" className="mt-2">
              <FormGroup className="d-flex flex-column">
                <Label className="text-uppercase letter-spacing mb-1">
                  status
                </Label>
                <CustomInput
                  type="radio"
                  id="exampleCustomRadio"
                  name="visibility"
                  inline
                  label="Private"
                  className="mb-1"
                  value="private"
                  checked={pipData?.visibility === "private" ? true : false}
                  onChange={handleChange}
                  // defaultChecked
                />
                <CustomInput
                  type="radio"
                  id="exampleCustomRadio2"
                  name="visibility"
                  inline
                  label="Share"
                  value="share"
                  checked={pipData?.visibility === "share" ? true : false}
                  onChange={handleChange}
                />
                <div className="validation">{errors?.visibility}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="my-2 mt-2">
              <Button className="common_button" onClick={() => saveIdea()}>
                Save Your PIP Idea
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  );
};

export default New_Idea;