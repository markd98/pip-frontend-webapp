import React, {useState, useEffect} from 'react'
import { selectThemeColors } from '@utils'
import Select from 'react-select'
import {
    Button,
    Card,
    CardBody,
    CardTitle,
    Col,
    CustomInput,
    FormGroup,
    Input,
    CardText,
    Label,
    Row,
  } from "reactstrap";
import { Link, useHistory } from 'react-router-dom'
import { ApiPost, ApiPostNoAuth, Bucket,ApiGet,ApiUpload } from "../../../helpers/API/ApiData";



  const data = [
    {
      id: 1,
      name: "Idea 1",
      pip: "Shared",
      price : "P2000",
      status  :"Pat Pend"
    },
    {
      id: 2,
      name: "Idea 2",
      price : "0",
      pip: "Shared",
      status  :"No Pat"
    },
    {
      id: 3,
      name: "Idea 3",
      pip: "Shared",
      price : "P10000",
      status  :"Patented"

    },
   
  ];

// const colourOptions = [
//   { value: 'India', label: 'India' },
//   { value: 'USA', label: 'USA' },
//   { value: 'Canada', label: 'Canada' }
// ]

const PipStatus = () => {
  const [status, setstatus] = useState([])
const [selectedStatus, setselectedStatus] = useState("")
const [list, setlist] = useState([])


  const getStatus =async () => {
    await ApiGet("/pip_status")
    .then((res) => {
      setstatus(res?.data?.data)
    })
    .catch((err) => {});

  }
  useEffect(async() => {
   await getStatus()
  }, [])

 let colourOptions = status?.map(v => {return  {value : v._id , label : v.name}})


 const handleChange=async (e) => {
  setselectedStatus(e.label)

  await ApiPost("/get_idea_by_status" , {status :e.label })
  .then((res) => {
    setlist(res?.data?.data[0]?.idea)
  })
  .catch((err) => {});
 }

    const history = useHistory()
    return (
        <><Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">PiP Status</CardTitle>
         <hr />
          <Row>
          <Col xl="12">
          <FormGroup>
                      <Label className="text-uppercase letter-spacing" for='register-username' >
                      Select Status
                      </Label>
                      <Select
                    theme={selectThemeColors}
                    className='react-select'
                    classNamePrefix='select'
                    defaultValue={colourOptions[0]}
                    name='clear'
                    options={colourOptions}
                    isClearable
                    onChange = {(e)=> handleChange(e)}
                  />
                    </FormGroup>
          </Col>

         <div className='mt-2 w-100'>
         {list.map((item, i) => (
            <Col xl="12">
              <FormGroup className=" row align-items-center mx-0 ">
                
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0 w-25 text-left cursor-pointer" onClick={()=> history.push(`/apps/blockChain?id=${item._id}&name=${item.name}`)}>{item.name}</p>
                  <p className="m-0 w-25 text-center">{item.visibility}</p>
                  <p className="m-0 w-25 text-center">{item.contributionTotalPIP}</p>
                  <p className="m-0 w-25 text-right cursor-pointer" onClick={()=> history.push(`/apps/pip-patent-status?id=${item._id}&name=${item.name}`)}>{selectedStatus}</p>
                </div>
              </FormGroup>
             
            </Col>
            
          ))}

         </div>
         <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={()=> history.push("/apps/newIdea")}>Add New PIP</Button>
          </Col>
          </Row>
          </CardBody>
          </Card>
          </>
    )
}

export default PipStatus
