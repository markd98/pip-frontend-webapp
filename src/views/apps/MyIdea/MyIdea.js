import React,{useEffect, useState} from "react";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { Link, useHistory } from 'react-router-dom'
import sort from "../../../assets/images/myImg/sort.png"
import { ApiPost, ApiPostNoAuth, Bucket,ApiGet } from "../../../helpers/API/ApiData";
import moment from "moment"
import {Edit  } from 'react-feather';




const MyIdea = () => {
    const [data, setdata] = useState([])
  const history = useHistory()

  useEffect(async() => {
    await ApiGet("/idea")
    .then((res) => {
      setdata(res?.data?.data)
    })
    .catch((err) => {});
  }, [])
  return <><Card className=" w-100 scroller">
  <CardBody>
    <CardTitle tag="h2">My Idea</CardTitle>
    <hr />
    <Row>
    
    <Col xl="12" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                <b className="m-0 w-16 text-left" >Name</b>
                  <b className="m-0 w-16 text-center">Status</b>
                  <b className="m-0 w-20 text-center">Company Name</b>
                  <b className="m-0 w-16 text-center">Date</b>
                  <b className="m-0 w-16 text-center">Funding</b>
                  <b className="m-0 w-16 text-right">Action</b>
                </div>
              </FormGroup>
             
            </Col> 
            {data.map((item, i) => (
             
            <Col xl="12">
              <FormGroup className=" row align-items-center mx-0 ">
                
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 " >
               
                  <p className="m-0 w-16 text-left" >{item?.name}</p>
                  <p className="m-0 w-16 text-center">{item?.visibility}</p>
                  <p className="m-0 w-20 text-center">{item?.company[0]?.name}</p>
                  <p className="m-0 w-16 text-center">{moment(item?.createdAt).format("DD-MM-YYYY")}</p>
                  {item?.isFundAdded  ?<p className="m-0 w-16 text-center blue fw-600 cursor-pointer" onClick={()=> history.push(`/apps/viewFunding?ideaId=${item._id}&fundId=${item?.funds[0]?._id}&name=${item.name}`)}>View Funding</p>
                   :   <p className="m-0 w-16 text-center blue fw-600 cursor-pointer" onClick={()=> history.push(`/apps/pip-idea-membership?id=${item?._id}&name=${item?.name}`)}>Add Funding</p>}
                
                  <p className="m-0 w-16 text-right "> {!item?.isBlockchainCommitted && <Edit className="cursor-pointer" onClick={()=> history.push(`/apps/editIdea?id=${item?._id}`)}/> }</p>
                </div>
                  {/* <Button className="common_button" onClick={() => history.push("/apps/newIdea")}>Edit</Button> */}
              </FormGroup>
             
            </Col>
            
          ))}
           
            </Row>
            </CardBody>
            </Card>
            </>
};

export default MyIdea;
