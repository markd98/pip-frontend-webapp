import React, { useState ,useEffect} from 'react'
import {  Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap'
import {
    Button,
    Card,
    CardBody,
    CardTitle,
    Col,
    CustomInput,
    FormGroup,
    Input,
    CardText,
    Label,
    Row,
  } from "reactstrap";
  import { selectThemeColors } from '@utils'
  import Select from 'react-select'
import { ApiPost } from '../../../helpers/API/ApiData';
import moment from "moment"
import queryString  from "query-string"
  const colourOptions = [
    { value: 'India', label: 'India' },
    { value: 'USA', label: 'USA' },
    { value: 'Canada', label: 'Canada' }
  ]

const ViewFunding = () => {

  const [data, setdata] = useState([])
  const [name, setname] = useState("")
  useEffect(async() => {
    const parsed = queryString.parse(location.search)
    
    await ApiPost("/fund_details_by_id", {ideaId : parsed?.ideaId , fundId : parsed?.fundId})

    .then((res) => {
      setdata(res?.data?.data[0])
      setname(parsed?.name)
      // setipcList(res?.data?.data);
    })
    .catch((err) => {});
  }, [])
  console.log("data",data);
    return (
        <><Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">View Funding</CardTitle>
         <hr/>
          <Row>
          <Col xl="6" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              PIP Idea name
              </Label>
              <Input
                type="text"
                id="basicInput"
                placeholder="My First Great PIP Idea"
                disabled
                value={name}
              />
            </FormGroup>
          </Col>
          <Col xl="6" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              estimated sales potential
              </Label>
              <Input
                type="text"
                id="basicInput"
                placeholder="$10000"
                disabled
                value={data?.estimatedSalesPotential}
              />
            </FormGroup>
          </Col>
          <Col xl="6" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              seeking PIPs/ $/ Eur/ yen
              </Label>
              <Input
                type="text"
                id="basicInput"
                placeholder="P50,000"
                disabled
                value={data?.seekingPIPs}

              />
            </FormGroup>
          </Col>
          
          <Col xl="6" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              starting ask to join in piips
              </Label>
              <Input
                type="text"
                id="basicInput"
                placeholder="P100"
                disabled
                value={data?.startingPIPs}

              />
            </FormGroup>
          </Col>
          <Col xl="6" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              raise ask amount per join
              </Label>
              <Input
                type="text"
                id="basicInput"
                placeholder="P10"
                disabled
                value={data?.raisePerJoinPIPs}

              />
            </FormGroup>
          </Col>
          <Col xl="6" className="mt-2">
    <FormGroup>
                <Label className="text-uppercase letter-spacing" for='register-username' >
                time limit to join
                </Label>
                <Input
                type="text"
                id="basicInput"
                placeholder="49%"
                disabled
                value={`${data?.timeLimit} Days`}
              />
              
              </FormGroup>
      </Col>
      <Col xl="6" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              percentage per p100
              </Label>
              <Input
                type="text"
                id="basicInput"
                placeholder="0.5%"
                disabled
                value={data?.percentagePerPIP}
              />
            </FormGroup>
          </Col> <Col xl="6" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              max percentage offered
              </Label>
              <Input
                type="text"
                id="basicInput"
                placeholder="49%"
                disabled
                value={data?.maxPercentageOffered}

              />
            </FormGroup>
          </Col>
          <Col xl="6" className="mt-2">
    <FormGroup>
                <Label className="text-uppercase letter-spacing" for='register-username' >
                funds lock-up period
                </Label>
                <Input
                type="text"
                id="basicInput"
                placeholder="49%"
                disabled
                value={`${data?.fundLockPeriod} Days`}

              />
               
              </FormGroup>
      </Col>
      <Col xl="6" className="mt-2 d-flex align-items-center">
          <FormGroup>
          <CustomInput type='radio' id='exampleCustomRadio' name='customRadio' checked={data?.isIdeaMarket} inline label='Post PIP In the Idea Market?'  />
       

          </FormGroup>
          </Col>






{data?.support_idea?.length > 0 &&          <Col xl="12 mt-2" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                <b className="m-0 w-25 text-left" >Name</b>
                  <b className="m-0 w-25 text-center">Date</b>
                  <b className="m-0 w-25 text-center">Joining PIP</b>
                  <b className="m-0 w-25 text-right">contribution In Percentage</b>
                </div>
              </FormGroup>
             
            </Col> 


    }
            {data?.support_idea?.map((item, i) => (
             
             <Col xl="12">
               <FormGroup className=" row align-items-center mx-0 ">
                 
                 <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 " >
                
                   <p className="m-0 w-25 text-left" >{item?.user[0]?.username}</p>
                   <p className="m-0 w-25 text-center"> {moment(item?.ideaFullList[0]?.createdAt).format("DD-MM-YYYY")}</p>
                   <p className="m-0 w-25 text-center">P{item?.ideaFullList[0]?.joiningPIPs}</p>
                   <p className="m-0 w-25 text-right">{item?.ideaFullList[0]?.contributionInPercentage}%</p>
                   </div>
                  
               </FormGroup>
              
             </Col>
             
           ))}


      <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={() => history.pushState("/apps/myIdea")}>Back</Button>
          </Col>
          </Row>
          </CardBody>
          </Card>
        
          </>
    )
}

export default ViewFunding
