import React,{useState} from 'react'
import {
    Button,
    Card,
    CardBody,
    CardTitle,
    Col,
    CustomInput,
    FormGroup,
    Input,
    CardText,
    Label,
    Row,
  } from "reactstrap";
import { DragDrop } from "@uppy/react";
import Uppy from "@uppy/core";
import thumbnailGenerator from "@uppy/thumbnail-generator";
import "uppy/dist/uppy.css";
import "@uppy/status-bar/dist/style.css";
import { Share2 } from 'react-feather'
import { Link, useHistory } from 'react-router-dom'



const PipDetail = () => {
    const [img, setImg] = useState(null);

    const history = useHistory()
  
    const uppy = new Uppy({
      meta: { type: "avatar" },
      restrictions: { maxNumberOfFiles: 1 },
      autoProceed: true,
    });
  
    uppy.use(thumbnailGenerator);
  
    uppy.on("thumbnail:generated", (file, preview) => {
      setImg(preview);
    });
    return (
        <><Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">PIP Detail</CardTitle>
          <hr />
          <Row>
          <Col xl="12" className="mt-2">
          <FormGroup>
              <div className='d-flex justify-content-between fw-600'>
              <Label className="text-uppercase letter-spacing" for='register-username' >
              Idea W
                </Label><Label className="text-uppercase letter-spacing" for='register-username' >
                Lorem Company
                </Label>
              </div>
          </FormGroup>
          </Col>
          <Col sm="12">
            <FormGroup>
              <DragDrop uppy={uppy} />
              {img !== null ? (
                <img className="rounded mt-2" src={img} alt="avatar" />
              ) : null}
            </FormGroup>
          </Col>
          <Col sm="12">
            <FormGroup>
             <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.</p>
            </FormGroup>
          </Col>
          <Col xl="12" className="mt-2">
          <FormGroup>
              <div className='d-flex justify-content-between fw-600'>
              <Label className="text-uppercase letter-spacing" for='register-username' >
              Human Necessities
                </Label><Label className="text-uppercase letter-spacing" for='register-username' >
                Patent Search
                </Label>
              </div>
          </FormGroup>
          </Col>
          <div className='d-flex justify-content-between align-items-center card-body'>

<CardText className='mb-0 text-uppercase '>Share for funding</CardText>
<div style={{padding: "6px",border: "1px solid #d8d6de",borderRadius: "0.357rem", cursor : "pointer"}} >

<Share2 size={19} />
</div>
</div>


<Col xl="12" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 cursor-pointer" onClick={()=> history.push("/apps/pip-idea-membership")}>
                  <p className="m-0">Idea Funding</p>
               
                </div>
              </FormGroup>
             
            </Col> <Col xl="12" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Current Sales Price</p>
                  <b className="m-0">$10000</b>
              
                </div>
              </FormGroup>
             
            </Col>
   


<div className='d-flex justify-content-between align-items-center card-body'>

<CardText className='mb-0 text-uppercase'>post on social media</CardText>
<div style={{padding: "6px",border: "1px solid #d8d6de",borderRadius: "0.357rem", cursor : "pointer"}} >

<Share2 size={19} />
</div>
</div>

<Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={() => history.push(" /apps/newIdea")}>Home</Button>
          </Col>
          </Row>
          </CardBody>
          </Card>
           </>
    )
}                           
export default PipDetail
