import React, { useState, useEffect } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Alert } from "reactstrap";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { selectThemeColors } from "@utils";
import { Link, useHistory } from "react-router-dom";

import Select from "react-select";
import { ApiGet } from "../../../helpers/API/ApiData";
import queryString from "query-string";
// const colourOptions = [
//   { value: 'India', label: 'India' },
//   { value: 'USA', label: 'USA' },
//   { value: 'Canada', label: 'Canada' }
// ]

const colourOptions = [];
for (let i = 0; i <= 50; i = i + 5) {
  if (i == 0) {
    colourOptions.push({ value: 1, label: "1 Days" });
  } else {
    colourOptions.push({ value: i, label: i + " Days" });
  }
}

const Month = [];
for (let i = 1; i <= 12; i++) {
  Month.push({ value: i, label: i + " Month" });
}

const Funding2 = (props) => {
  const [centeredModal, setCenteredModal] = useState(false);
  const [fundingData, setfundingData] = useState([]);
  const [errors, setError] = useState({});
  const [ID, setID] = useState("");
  const parsed = queryString.parse(props.location.search);

  const getFunding = async () => {
    await ApiGet(`/idea/${parsed.id}`)
      .then((res) => {
        setfundingData(res.data.data);
        // setDetails(res?.data?.data);
      })
      .catch((err) => {});
  };

  useEffect(async () => {
    // setID(parsed.id);
    await getFunding();
  }, []);

  const history = useHistory();


  return (
    <>
      <Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">PIP Idea Membership Funding Details</CardTitle>
          <hr />
          <Row>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  Add pip idea for funding
                </Label>
                <Input
                  type="text"
                  id="basicInput"
                  placeholder="My First Great PIP Idea"
                  name="MembershipName"
                  value={fundingData[0]?.name}
                  disabled
                  // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.MembershipName}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  estimated sales potential
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="$10000"
                  name="estimatedSalesPotential"
                  value={fundingData[0]?.fund[0]?.estimatedSalesPotential}
                  disabled
                  // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.estimatedSalesPotential}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  seeking PIPs/ $/ Eur/ yen
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="P50,000"
                  name="seekingPIPs"
                  value={fundingData[0]?.fund[0]?.seekingPIPs}
                  disabled
                  // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.seekingPIP}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <CustomInput
                  disabled
                  type="checkbox"
                  id="exampleCustomRadio"
                  name="customRadio"
                  inline
                  label="Post PIP In the Idea Market?"
                  value={fundingData[0]?.fund[0]?.isIdeaMarket}
                  checked= {fundingData[0]?.fund[0]?.isIdeaMarket ? true : false}

                  // onChange={(e) =>
                  //   setfundingData({
                  //     ...fundingData,
                  //     isIdeaMarket: e.target.checked,
                  //   })
                  // }
                />
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  starting ask to join in pips
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="P100"
                  name="startingPIPs"
                  value={fundingData[0]?.fund[0]?.startingPIPs}
                  disabled
                  // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.startingPIPs}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  raise ask amount per join
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="P10"
                  name="raisePerJoinPIPs"
                  value={fundingData[0]?.fund[0]?.raisePerJoinPIPs}
                  disabled
                  // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.raisePerJoinPIPs}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  className="text-uppercase letter-spacing"
                  for="register-username"
                >
                  time limit to join
                </Label>
                <Input
                  type="text"
                  id="basicInput"
                  placeholder="P10"
                  name="raisePerJoinPIPs"
                  value={fundingData[0]?.fund[0]?.timeLimit + " Days"}
                  disabled
                  // onChange={handleChange}
                />
                {/* {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null} */}
                {/* <div className='validation'>
                  {errors?.timeLimit}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  percentage per p100
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="0.5%"
                  name="percentagePerPIP"
                  value={fundingData[0]?.fund[0]?.percentagePerPIP}
                  disabled
                  // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.percentagePerPIP}
                </div> */}
              </FormGroup>
            </Col>{" "}
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  max percentage offered
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="49%"
                  name="maxPercentageOffered"
                  value={fundingData[0]?.fund[0]?.maxPercentageOffered}
                  disabled
                  // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.maxPercentageOffered}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  className="text-uppercase letter-spacing"
                  for="register-username"
                >
                  funds lock-up period
                </Label>
                <Input
                  type="text"
                  id="basicInput"
                  placeholder="P10"
                  name="raisePerJoinPIPs"
                  value={fundingData[0]?.fund[0]?.fundLockPeriod + " Month"}
                  disabled
                  // onChange={handleChange}
                />
                {/* {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null} */}

                {/* <div className='validation'>
                  {errors?.fundLockPeriod}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="my-2 mt-2">
              <Button
                className="common_button"
                 onClick={() => history.goBack()}
              >
                Back
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
      {/* <Modal isOpen={centeredModal} toggle={() => setCenteredModal(!centeredModal)} className='modal-dialog-centered'>
          <ModalBody className="text-center my-4 ">
         <b style={{fontSize : "18px"}}> Your PiP Idea is on its way!</b>
          </ModalBody>
          <ModalFooter>
            <Button color='primary' className="common_button"  onClick={() => {setCenteredModal(!centeredModal); history.push("newIdea")}}>
              Done
            </Button>{' '}
          </ModalFooter>
        </Modal> */}
    </>
  );
};

export default Funding2;
