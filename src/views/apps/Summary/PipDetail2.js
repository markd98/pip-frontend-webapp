import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  CardBody,
  Col,
  CustomInput,
  FormGroup,
  Input,
  Label,
  Row,
  CardTitle,
} from "reactstrap";
import Uppy from "@uppy/core";
import thumbnailGenerator from "@uppy/thumbnail-generator";
import { DragDrop } from "@uppy/react";
import Select, { components } from "react-select";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToMarkdown from "draftjs-to-markdown";
import { convertToRaw } from "draft-js";
import "uppy/dist/uppy.css";
import "@uppy/status-bar/dist/style.css";
import "@styles/react/libs/file-uploader/file-uploader.scss";
import "@styles/react/libs/editor/editor.scss";
import { NavLink, useHistory } from "react-router-dom";
import { Save, Share2 } from "react-feather";
import {
  ApiPost,
  ApiPostNoAuth,
  Bucket,
  ApiGet,
  ApiUpload,
} from "../../../helpers/API/ApiData";
import RichTextEditor from "react-rte";
import Dropzone from "react-dropzone";
import { ToastContainer, toast } from "react-toastify";
import { Modal, ModalHeader, ModalBody, ModalFooter, Alert } from "reactstrap";
import queryString from "query-string";
// import CancelIcon from "@material-ui/icons/Cancel";
import {
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TelegramShareButton,
  TelegramIcon,
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
} from "react-share";
const PipDetail2 = (props) => {
  const [details, setDetails] = useState([]);
  const [ipcList, setipcList] = useState([]);
  const [companyList, setcompanyList] = useState([]);
  const [centeredModal, setCenteredModal] = useState(false)
  const [centeredModal2, setCenteredModal2] = useState(false)
  const history = useHistory();
  const parsed = queryString.parse(props.location.search);
  const getIpc = async () => {
    await ApiGet(`/idea/${parsed.id}`)
      .then((res) => {
        setDetails(res?.data?.data);
      })
      .catch((err) => { });
    await ApiGet("/ipc")
      .then((res) => {
        setipcList(res?.data?.data);
      })
      .catch((err) => { });
    await ApiGet("/company")
      .then((res) => {
        setcompanyList(res?.data?.data);
      })
      .catch((err) => { });
  };
  useEffect(async () => {
    await getIpc();
    localStorage.setItem("isValidate", false);
  }, []);
  let colorOptions = ipcList?.map((v) => {
    return { value: v._id, label: v.name };
  });
  let colorOptions1 = companyList?.map((v) => {
    return { value: v._id, label: v.name };
  });
  const openFunding = async () => {
    // localStorage.setItem("funding2", JSON.stringify(details[0]));
    history.push(`/apps/pip-idea-funding?id=${details[0]._id}`);
  };
  let selected = companyList?.find((v) => v._id === details[0]?.companyId);
  let selected2 = ipcList?.find((v) => v._id === details[0]?.ipcId);
  return (
    <>
      <Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">PIP Details</CardTitle>
          <hr />
          <Row>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  name your pip
                </Label>
                <Input
                  type="name"
                  id="basicInput"
                  placeholder="My First Great PIP Idea"
                  name="name"
                  disabled
                  value={details[0]?.name}
                // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.name}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  Explain your new idea
                </Label>
                {/* <Editor
                editorState={value}
                onEditorStateChange={(data) =>handleDesc(data) }
              /> */}
                <Input
                  type="textarea"
                  id="basicInput"
                  placeholder="My First Great PIP Idea"
                  name="name"
                  disabled
                  value={details[0]?.description
                    ?.toString()
                    ?.replace(/<[^>]+>/g, "")}
                // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.description}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup className="d-flex flex-column">
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  Document
                </Label>
                {/* <Editor
                editorState={value}
                onEditorStateChange={(data) =>handleDesc(data) }
              /> */}
                <img
                  width="300px"
                  // height="1500px"
                  className="rounded"
                  src={Bucket + details[0]?.document[0]}
                />
                {/* <div className='validation'>
                  {errors?.description}
                </div> */}
              </FormGroup>
            </Col>
            <Col sm="12" >
              <FormGroup></FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  Select Company if applicable
                </Label>
                <Input
                  type="text"
                  id="basicInput"
                  placeholder="My First Great PIP Idea"
                  name="name"
                  disabled
                  value={selected?.name}
                // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.companyId}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  select IPC
                </Label>
                <Input
                  type="text"
                  id="basicInput"
                  placeholder="My First Great PIP Idea"
                  name="name"
                  disabled
                  value={selected2?.name}
                // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.ipcId}
                </div> */}
              </FormGroup>
            </Col>
            {/* <Col xl="12" className="mt-2">
              <FormGroup className="text-center">
                <NavLink
                  to={"/apps/services"}
                  className="text-uppercase letter-spacing"
                >
                  Click for PIP Services
                </NavLink>
               
              </FormGroup>
              {purchasedService.length > 0 &&<Label for="basicInput" className="text-uppercase letter-spacing">
            Purchased Services
              </Label>}
              <Row>
                {purchasedService?.map((item, i) => (
            <Col xl="12">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">{item.name}</p>
                  <b className="m-0">{item.price} PIP</b>
                </div>
              </FormGroup>
             
            </Col>
            
          ))}
              </Row>
            </Col> */}
            <Col xl="12" className="mt-2">
              <FormGroup>
                <div className="d-flex justify-content-between align-items-center ">
                  <Label className="text-uppercase letter-spacing">
                    Share for funding
                  </Label>
                  <div
                    style={{
                      padding: "6px",
                      border: "1px solid #d8d6de",
                      borderRadius: "0.357rem",
                      cursor: "pointer",
                    }}
                    onClick={() => setCenteredModal2(!centeredModal2)}

                  >
                    <Share2 size={19} />
                  </div>
                </div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <div
                  className="border rounded row justify-content-between m-0 px-1 py-50 cursor-pointer"
                  onClick={() => openFunding()}
                >
                  <p className="m-0">Idea Funding</p>
                </div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                {/* <div className="border rounded row justify-content-between m-0 px-1 py-50"> */}
                {/* <p className="m-0">Current Sales Price</p> */}
                {/* <b className="m-0">$10000</b> */}
                <Input
                  type="text"
                  id="basicInput"
                  placeholder="Current Sales Price"
                  name="salesPrice"
                  disabled
                  value={details[0]?.salesPrice}
                // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.salesPrice}
                </div> */}
                {/* </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <div className="d-flex justify-content-between align-items-center ">
                  <Label className="text-uppercase letter-spacing">
                    post on social media
                  </Label>
                  <div
                    style={{
                      padding: "6px",
                      border: "1px solid #d8d6de",
                      borderRadius: "0.357rem",
                      cursor: "pointer",
                    }}
                    onClick={() => setCenteredModal(!centeredModal)}
                  >
                    <Share2 size={19} />
                  </div>
                </div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup className="d-flex flex-column">
                <Label className="text-uppercase letter-spacing mb-1">
                  status
                </Label>
                <CustomInput
                  type="radio"
                  id="exampleCustomRadio"
                  name="visibility"
                  inline
                  label="Private"
                  className="mb-1"
                  value="private"
                  checked={details[0]?.visibility === "private" ? true : false}
                // onChange={handleChange}
                // defaultChecked
                />
                <CustomInput
                  type="radio"
                  id="exampleCustomRadio2"
                  name="visibility"
                  inline
                  label="Share"
                  value="share"
                  checked={details[0]?.visibility === "share" ? true : false}
                // onChange={handleChange}
                />
                {/* <div className='validation'>
                  {errors?.visibility}
                </div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="my-2 mt-2">
              <Button
                className="common_button"
                onClick={() => history.goBack()}
              >
                Back
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Modal isOpen={centeredModal} toggle={() => setCenteredModal(!centeredModal)} className='modal-dialog-centered modal-xs'>
        <ModalHeader toggle={() => setCenteredModal(!centeredModal)} >POST ON SOCIAL MEDIA</ModalHeader>
        <ModalBody>
          <div className="d-flex justify-content-around">
            <FacebookShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <FacebookIcon
                size={30}
              />
            </FacebookShareButton>
            <EmailShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <EmailIcon
                size={30}
              />
            </EmailShareButton>
            <LinkedinShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <LinkedinIcon
                size={30}
              />
            </LinkedinShareButton>
          </div>
          <div className="d-flex justify-content-around mt-2">
            <TwitterShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <TwitterIcon
                size={30}

              />
            </TwitterShareButton>
            <TelegramShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <TelegramIcon
                size={30}
              />
            </TelegramShareButton>
            <WhatsappShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <WhatsappIcon
                size={30}
                url="https://main.db3p40161540r.amplifyapp.com/"
              />
            </WhatsappShareButton>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button color='primary' onClick={() => setCenteredModal(!centeredModal)}>
            Cancel
          </Button>{' '}
        </ModalFooter>
      </Modal>
      <Modal isOpen={centeredModal2} toggle={() => setCenteredModal2(!centeredModal2)} className='modal-dialog-centered modal-xs'>
        <ModalHeader  >SHARE FOR FUNDING</ModalHeader>
        <ModalBody>
          <div className="d-flex justify-content-around">
            <FacebookShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <FacebookIcon
                size={30}
              />
            </FacebookShareButton>
            <EmailShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <EmailIcon
                size={30}
              />
            </EmailShareButton>
            <LinkedinShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <LinkedinIcon
                size={30}
              />
            </LinkedinShareButton>
          </div>
          <div className="d-flex justify-content-around mt-2">
            <TwitterShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <TwitterIcon
                size={30}
              />
            </TwitterShareButton>
            <TelegramShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <TelegramIcon
                size={30}
              />
            </TelegramShareButton>
            <WhatsappShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <WhatsappIcon
                size={30}
              />
            </WhatsappShareButton>
          </div>


        </ModalBody>
        <ModalFooter>
          <Button color='primary' onClick={() => setCenteredModal2(!centeredModal2)}>
            Cancel
          </Button>{' '}
        </ModalFooter>
      </Modal>
    </>
  );
};
export default PipDetail2;