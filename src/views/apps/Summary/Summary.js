import React,{useState, useEffect} from "react";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { Camera } from 'react-feather'
import { Link, useHistory } from 'react-router-dom'
import { ApiPost, ApiPostNoAuth, Bucket,ApiGet } from "../../../helpers/API/ApiData";



  const data = [
    {
      id: 1,
      name: "Current PIP Balance",
      pip: "P12,321",
    },
    {
      id: 2,
      name: "PIP Account",
      pip: "P1769",
    },
    {
      id: 3,
      name: "Your PIP Funds",
      pip: "P8000",
    },
    {
      id: 4,
      name: "Your PIP Memberships",
      pip: "P2552",
    },
    {
      id: 5,
      name: "Withdraw PIPs - Amount",
      pip: "P1500",
    },
  ];
const Summary = () => {
  const [data1, setdata1] = useState([])


  const history = useHistory()

  useEffect(async() => {
    await ApiGet("/summery")
      .then((res) => {
        setdata1(res?.data?.data)
      })
      .catch((err) => {});
  }, [])


  return <><Card className=" w-100 scroller">
  <CardBody>
    <CardTitle tag="h2">PIP Summary</CardTitle>
    <hr />
    <Row>
    <Col xl="12">
    <FormGroup>
    <Label className="text-uppercase letter-spacing" for='register-username' >
    Your pips
                </Label>
    </FormGroup>
    </Col>
    <Col xl="12" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <b className="m-0">PIP ID</b>
                  <b className="m-0">Status</b>
                  <b className="m-0">Value</b>
                </div>
              </FormGroup>
             
            </Col> 
            <Col xl="12"  >
              {data1?.idea?.map(v => {
                return <>
                <FormGroup className=" row align-items-center mx-0 ">
              
              <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 cursor-pointer" onClick={()=> history.push(`/apps/pip-detail2?id=${v._id}`)}>
                <p className="m-0 w-33">{v.name}</p>
                <p className="m-0 w-33 text-center">{v.visibility}</p>
                <p className="m-0 w-33 text-right">P{v?.fund[0]?.seekingPIPs}</p>
              </div>
              </FormGroup>
              </>
              })}
              
             
            </Col> 
           
            <Col xl="12" className="mt-2">
    <FormGroup>
 <Label className="text-uppercase letter-spacing" for='register-username' >
 Your pip support memberships
                </Label>
    </FormGroup>
    </Col>
    <Col xl="12" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <b className="m-0">PIP ID</b>
                  <b className="m-0">Level</b>
                  <b className="m-0">Value</b>
                </div>
              </FormGroup>
             
            </Col> 
    <Col xl="12" >
    {data1?.support?.length > 0 && data1?.support[0]?.idea?.map((v,i) => {
                return <>
                <FormGroup className=" row align-items-center mx-0 ">
              
              <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 " >
                <p className="m-0 w-33">{v.name}</p>
                <p className="m-0 w-33 text-center">{v?.fund[0]?.percentageOfCompletion}%</p>
                <p className="m-0 w-33 text-right">P{v?.fund[0]?.startingPIPs}</p>
              </div>
              </FormGroup>
              </>
              })}
       
             
           
            </Col>
            <Col xl="12" className="mt-2">
           
              <FormGroup className=" row align-items-center mx-0 ">
               
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Current PIP Balance</p>
                  <b className="m-0">P{data1.currentPIPBalance}</b>
                </div>
               
              </FormGroup>

              <FormGroup className=" row align-items-center mx-0 ">
               
              <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">PIP Account</p>
                  <b className="m-0">P{data1.PIPAccount}</b>
                </div>
              
             </FormGroup>

             <FormGroup className=" row align-items-center mx-0 ">
               
             <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Your PIP Funds</p>
                  <b className="m-0">P{data1.youPIPFunds}</b>
                </div>
               
              </FormGroup>

              <FormGroup className=" row align-items-center mx-0 ">
               
              <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Your PIP Memberships</p>
                  <b className="m-0">P{data1.yourPIPMembership}</b>
                </div>
                 
                </FormGroup>
  
                <FormGroup className=" row align-items-center mx-0 ">
          <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Withdraw PIPs - Amount</p>
                  <b className="m-0">P{data1.withdrawalPIPAmount}</b>
                </div>
                </FormGroup>
             
             
     
             </Col>
             <Label className="text-uppercase letter-spacing card-body" for='register-username' >
             p100 = 0.02 eth
                </Label>
               
                <div className='d-flex justify-content-end align-items-center card-body'>

                <Label className="text-uppercase letter-spacing mr-2" for='register-username' >
                scan qr code for wallet
                </Label>
              <div style={{padding: "6px",border: "1px solid #d8d6de",borderRadius: "0.357rem", cursor : "pointer"}} >

              <Camera size={19}/>
              </div>
              </div>
              <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={() =>history.push("/")}>Home</Button>
          </Col>
    </Row>
    </CardBody>
    </Card>
    </>
};

export default Summary;
