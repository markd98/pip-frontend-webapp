import React, { useState, useEffect } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Alert } from "reactstrap";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { selectThemeColors } from "@utils";
import { Link, useHistory } from "react-router-dom";
import queryString from "query-string";
import Select from "react-select";
import {
  EmailShareButton,
  EmailIcon,
  FacebookShareButton,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TelegramShareButton,
  TelegramIcon,
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
} from "react-share";
import { Save, Share2 } from "react-feather";
import { ApiPost } from "../../../helpers/API/ApiData";
import { toast, ToastContainer } from "react-toastify";
// const colourOptions = [
//   { value: 'India', label: 'India' },
//   { value: 'USA', label: 'USA' },
//   { value: 'Canada', label: 'Canada' }
// ]

const colourOptions = [];
for (let i = 0; i <= 50; i = i + 5) {
  if (i == 0) {
    colourOptions.push({ value: 1, label: "1 Days" });
  } else {
    colourOptions.push({ value: i, label: i + " Days" });
  }
}

const Month = [];
for (let i = 1; i <= 12; i++) {
  Month.push({ value: i, label: i + " Month" });
}

const Funding = () => {
  const [centeredModal, setCenteredModal] = useState(false);
  const [centeredModal2, setCenteredModal2] = useState(false);
  const [centeredModal3, setCenteredModal3] = useState(false);
  const [fundingData, setfundingData] = useState({ isIdeaMarket: false });
  const [errors, setError] = useState({});
  const ideaId = queryString.parse(window.location.search);
  useEffect(() => {
    if (localStorage.getItem("funding"))
      setfundingData(JSON.parse(localStorage.getItem("funding")));
  }, []);

  const history = useHistory();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setfundingData({
      ...fundingData,
      [name]: value,
    });
  };

  const validateForm = () => {
    let errors = {};
    let formIsValid = true;

    // if (!fundingData.MembershipName) {
    //   formIsValid = false;
    //   errors["MembershipName"] = "Membership Name is requried";
    // }
    if (!fundingData.estimatedSalesPotential) {
      formIsValid = false;
      errors["estimatedSalesPotential"] =
        "Estimated Sales Potential is requried";
    }
    if (!fundingData.fundLockPeriod) {
      formIsValid = false;
      errors["fundLockPeriod"] = "Fund Lock Period is Requried";
    }
    if (!fundingData.maxPercentageOffered) {
      formIsValid = false;
      errors["maxPercentageOffered"] = "Max Percentage Offered is Requried";
    }
    if (!fundingData.percentagePerPIP) {
      formIsValid = false;
      errors["percentagePerPIP"] = "Percentage Per PIP is Requried";
    }
    if (!fundingData.raisePerJoinPIPs) {
      formIsValid = false;
      errors["raisePerJoinPIPs"] = "Raise Per Join PIPs is Requried";
    }
    if (!fundingData.seekingPIPs) {
      formIsValid = false;
      errors["seekingPIP"] = "Seeking PIPs is Requried";
    }
    if (!fundingData.startingPIPs) {
      formIsValid = false;
      errors["startingPIPs"] = "Starting PIPs is Requried";
    }
    if (!fundingData.timeLimit) {
      formIsValid = false;
      errors["timeLimit"] = "Time Limit is Requried";
    }

    setError(errors);
    return formIsValid;
  };


  const getPip = () => {
    if (validateForm()) {
      const body = {
        ...fundingData,
        ideaId: ideaId.id,
      };
      ApiPost("/fund", body)
        .then((res) => {
          setCenteredModal3(!centeredModal3);
          toast.success(res.data.message);
        })
        .catch((err) => {
          toast.error(err.message);
        });
      // setCenteredModal(!centeredModal)
      localStorage.setItem("isValidate", true);
      // localStorage.setItem("funding", JSON.stringify(fundingData))
    }
  };
  return (
    <>
      <ToastContainer />
      <Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">PIP Idea Membership Funding</CardTitle>
          <hr />
          <Row>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  Add pip idea for funding
                </Label>
                <Input
                  type="text"
                  id="basicInput"
                  placeholder="My First Great PIP Idea"
                  name="MembershipName"
                  value={ideaId.name}
                  // onChange={handleChange}
                  disabled
                />
                {/* <div className="validation">{errors?.MembershipName}</div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  estimated sales potential
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="$10000"
                  name="estimatedSalesPotential"
                  value={fundingData.estimatedSalesPotential}
                  onChange={handleChange}
                />
                <div className="validation">
                  {errors?.estimatedSalesPotential}
                </div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  seeking PIPs
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="P50,000"
                  name="seekingPIPs"
                  value={fundingData.seekingPIPs}
                  onChange={handleChange}
                />
                <div className="validation">{errors?.seekingPIP}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <CustomInput
                  type="checkbox"
                  id="exampleCustomRadio"
                  name="customRadio"
                  inline
                  label="Post PIP In the Idea Market?"
                  onChange={(e) =>
                    setfundingData({
                      ...fundingData,
                      isIdeaMarket: e.target.checked,
                    })
                  }
                />
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  starting ask to join in piips
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="P100"
                  name="startingPIPs"
                  value={fundingData.startingPIPs}
                  onChange={handleChange}
                />
                <div className="validation">{errors?.startingPIPs}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  raise ask amount per join
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="P10"
                  name="raisePerJoinPIPs"
                  value={fundingData.raisePerJoinPIPs}
                  onChange={handleChange}
                />
                <div className="validation">{errors?.raisePerJoinPIPs}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  className="text-uppercase letter-spacing"
                  for="register-username"
                >
                  time limit to join
                </Label>
                <Select
                  theme={selectThemeColors}
                  className="react-select"
                  classNamePrefix="select"
                  defaultValue={colourOptions[0]}
                  options={colourOptions}
                  isClearable
                  name="timeLimit"
                  // value={pipData.ipcId}
                  onChange={(e) =>
                    setfundingData({ ...fundingData, timeLimit: e.value })
                  }
                />
                {/* {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null} */}
                <div className="validation">{errors?.timeLimit}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  percentage per p100
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="0.5%"
                  name="percentagePerPIP"
                  value={fundingData.percentagePerPIP}
                  onChange={handleChange}
                />
                <div className="validation">{errors?.percentagePerPIP}</div>
              </FormGroup>
            </Col>{" "}
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                  max percentage offered
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="49%"
                  name="maxPercentageOffered"
                  value={fundingData.maxPercentageOffered}
                  onChange={handleChange}
                />
                <div className="validation">{errors?.maxPercentageOffered}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  className="text-uppercase letter-spacing"
                  for="register-username"
                >
                  funds lock-up period
                </Label>
                <Select
                  theme={selectThemeColors}
                  className="react-select"
                  classNamePrefix="select"
                  defaultValue={Month[0]}
                  options={Month}
                  isClearable
                  name="fundLockPeriod"
                  // value={pipData.ipcId}
                  onChange={(e) =>
                    setfundingData({ ...fundingData, fundLockPeriod: e.value })
                  }
                />
                {/* {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null} */}

                <div className="validation">{errors?.fundLockPeriod}</div>
              </FormGroup>
            </Col>
            <Col xl="12" className="my-2 mt-2">
              <Button className="common_button" onClick={() => getPip()}>
                Get Your PIP Funded
              </Button>
            </Col>
          </Row>
        </CardBody>
      </Card>
      {/* <Modal
        isOpen={centeredModal}
        toggle={() => setCenteredModal(!centeredModal)}
        className="modal-dialog-centered"
      >
        <ModalBody className="text-center my-4 ">
          <b style={{ fontSize: "18px" }}> Your PiP Idea is on its way!</b>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            className="common_button"
            onClick={() => {
              setCenteredModal(!centeredModal);
              history.push("newIdea");
            }}
          >
            Done
          </Button>{" "}
        </ModalFooter>
      </Modal> */}
      <Modal
        isOpen={centeredModal}
        toggle={() => setCenteredModal(!centeredModal)}
        className="modal-dialog-centered modal-xs"
      >
        <ModalHeader toggle={() => setCenteredModal(!centeredModal)}>
          POST ON SOCIAL MEDIA
        </ModalHeader>
        <ModalBody>
          <div className="d-flex justify-content-around">
            <FacebookShareButton
              onClick={() => setCenteredModal(!centeredModal)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <FacebookIcon
                onClick={() => setCenteredModal(!centeredModal)}
                size={30}
              />
            </FacebookShareButton>
            <EmailShareButton
              onClick={() => setCenteredModal(!centeredModal)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <EmailIcon
                onClick={() => setCenteredModal(!centeredModal)}
                size={30}
              />
            </EmailShareButton>
            <LinkedinShareButton
              onClick={() => setCenteredModal(!centeredModal)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <LinkedinIcon
                onClick={() => setCenteredModal(!centeredModal)}
                size={30}
              />
            </LinkedinShareButton>
          </div>
          <div className="d-flex justify-content-around mt-2">
            <TwitterShareButton
              onClick={() => setCenteredModal(!centeredModal)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <TwitterIcon
                onClick={() => setCenteredModal(!centeredModal)}
                size={30}
              />
            </TwitterShareButton>
            <TelegramShareButton
              onClick={() => setCenteredModal(!centeredModal)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
            >
              <TelegramIcon
                onClick={() => setCenteredModal(!centeredModal)}
                size={30}
              />
            </TelegramShareButton>
            <WhatsappShareButton
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/"
              onClick={() => setCenteredModal(!centeredModal)}
            >
              <WhatsappIcon
                onClick={() => setCenteredModal(!centeredModal)}
                size={30}
                url="https://main.db3p40161540r.amplifyapp.com/"
              />
            </WhatsappShareButton>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => setCenteredModal(!centeredModal)}
          >
            Cancel
          </Button>{" "}
        </ModalFooter>
      </Modal>

      <Modal
        isOpen={centeredModal2}
        toggle={() => setCenteredModal2(!centeredModal2)}
        className="modal-dialog-centered modal-xs"
      >
        <ModalHeader >
          SHARE FOR FUNDING
        </ModalHeader>
        <ModalBody>
          <div className="d-flex justify-content-around">
            <FacebookShareButton
              onClick={() => setCenteredModal2(!centeredModal2)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <FacebookIcon
                onClick={() => setCenteredModal2(!centeredModal2)}
                size={30}
              />
            </FacebookShareButton>
            <EmailShareButton
              onClick={() => setCenteredModal2(!centeredModal2)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <EmailIcon
                onClick={() => setCenteredModal2(!centeredModal2)}
                size={30}
              />
            </EmailShareButton>
            <LinkedinShareButton
              onClick={() => setCenteredModal2(!centeredModal2)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <LinkedinIcon
                onClick={() => setCenteredModal2(!centeredModal2)}
                size={30}
              />
            </LinkedinShareButton>
          </div>
          <div className="d-flex justify-content-around mt-2">
            <TwitterShareButton
              onClick={() => setCenteredModal2(!centeredModal2)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <TwitterIcon
                onClick={() => setCenteredModal2(!centeredModal2)}
                size={30}
              />
            </TwitterShareButton>
            <TelegramShareButton
              onClick={() => setCenteredModal2(!centeredModal2)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <TelegramIcon
                onClick={() => setCenteredModal2(!centeredModal2)}
                size={30}
              />
            </TelegramShareButton>
            <WhatsappShareButton
              onClick={() => setCenteredModal2(!centeredModal2)}
              quote="PIp"
              url="https://main.db3p40161540r.amplifyapp.com/apps/pip-idea-membership"
            >
              <WhatsappIcon
                onClick={() => setCenteredModal2(!centeredModal2)}
                size={30}
              />
            </WhatsappShareButton>
          </div>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => setCenteredModal2(!centeredModal2)}
          >
            Cancel
          </Button>{" "}
        </ModalFooter>
      </Modal>

      <Modal
        isOpen={centeredModal3}
        toggle={() => setCenteredModal3(!centeredModal3)}
        className="modal-dialog-centered modal-md"
      >
        <ModalHeader >
          SHARE FOR FUNDING
        </ModalHeader>
        <ModalBody>
          <Col xl="12" className="mt-2">
            <FormGroup>
              <div className="d-flex justify-content-between align-items-center ">
                <Label className="text-uppercase letter-spacing">
                  Share for funding
                </Label>
                <div
                  style={{
                    padding: "6px",
                    border: "1px solid #d8d6de",
                    borderRadius: "0.357rem",
                    cursor: "pointer",
                  }}
                  onClick={() => setCenteredModal2(!centeredModal2)}
                >
                  <Share2 size={19} />
                </div>
              </div>
            </FormGroup>
          </Col>
          <Col xl="12" className="mt-2">
            <FormGroup>
              <div className="d-flex justify-content-between align-items-center ">
                <Label className="text-uppercase letter-spacing">
                  post on social media
                </Label>
                <div
                  style={{
                    padding: "6px",
                    border: "1px solid #d8d6de",
                    borderRadius: "0.357rem",
                    cursor: "pointer",
                  }}
                  onClick={() => setCenteredModal(!centeredModal)}
                >
                  <Share2 size={19} />
                </div>
              </div>
            </FormGroup>
          </Col>
        </ModalBody>
        <ModalFooter>
          <Button
            className="common_button"
            // onClick={() => setCenteredModal2(!centeredModal2)}
            onClick={() => history.push("/apps/myIdea")}
          >
            Done
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </>
  );
};

export default Funding;