import React, { useEffect, useRef } from 'react'
import { ApiPost, ApiPostNoAuth, Bucket, ApiGet } from "../../../helpers/API/ApiData";
import { Link, useHistory } from 'react-router-dom'


const Payment = ({ total }) => {
  const paypal = useRef()
  const history = useHistory()

  console.log("total", total);

  useEffect(() => {



    window.paypal.Buttons({
      style: {
        layout: 'vertical',
        color: 'blue',
        shape: 'rect',
        label: 'paypal',
        height: 36,
      },

      createOrder: function (data, actions) {

        // This function sets up the details of the transaction, including the amount and line item details.
        return actions.order.create({


          purchase_units: [{
            //  item_list : items,
            amount: {
              value: total
              // parseInt(price*10)*0.1
            }
          }]
        });
      },
      onApprove: async (data, actions) => {
        const order = await actions.order.capture()
        let body = {
          // pipId:checkid,
          pipCredits: `P${total / 0.05}`,
          amount: total,
          paymentURL: order?.links[0]?.href,
          paymentType: 2,
          // itemList:items
        }
        await ApiPost("/paypal/payment_check", body)
          .then((res) => {
            // getServices()
            setTimeout(() => {

              history.push("/")
            }, 2000);
            //   window.location.href = res?.data?.forwardLink
          })
          .catch((err) => { });

      },
      onError: (ree) => {
      }

    }).render(paypal.current);
  }, [])

  return (
    <div>
      <div ref={paypal}></div>
    </div>
  )
}

export default Payment
