import React,{useState,useEffect} from 'react'
import { useHistory } from 'react-router-dom'
import { Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap'


import {
    Button,
    Card,
    CardBody,
    CardTitle,
    Col,
    CustomInput,
    FormGroup,
    Input,
    CardText,
    Label,
    Row,
  } from "reactstrap";
  import { selectThemeColors } from '@utils'
  import Select from 'react-select'
  import { Share2 } from 'react-feather'
import Payment from './Payment';
import { ApiGet } from '../../../helpers/API/ApiData';
  
  
  const colourOptions = [
    { value: '100', label: 'P100' },
    { value: '200', label: 'P200' },
    { value: '300', label: 'P300' }
  ]

const BuyPIP = () => {
  const [ispayment, setispayment] = useState(false)
  const [selectedVlaue, setselectedVlaue] = useState(100)
  const [handleInput, sethandleInput] = useState(null)
  const [data1, setdata1] = useState([])



  const history = useHistory()

  useEffect(async() => {
    await ApiGet("/summery")
      .then((res) => {
        setdata1(res?.data?.data)
      })
      .catch((err) => {});
  }, [])

  return <>
  <Card className=" w-100 scroller">
  <CardBody>
    <CardTitle tag="h2">Buy PIP Tokens</CardTitle>
    <hr />
    <Row>
    
    <Col xl="12"  className="mt-2">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Current Pip Balance</p>
                  <b className="m-0">P{data1.currentPIPBalance}</b>
                </div>
              </FormGroup>
             
            </Col>

            <Col xl="12 mt-1">
    <FormGroup>
                <Label className="text-uppercase letter-spacing" for='register-username' >
                purchase pip credits
                </Label>
                <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              defaultValue={colourOptions[0]}
              name='clear'
              options={colourOptions}
              isClearable
              onChange={(e)=> setselectedVlaue(e.value)}
            />
                {/* {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null} */}
              </FormGroup>
    </Col>
    <Label className="text-uppercase letter-spacing text-center w-100" for='register-username' >
                or
                </Label>
                <Col xl="12" className="mt-2">
              <FormGroup>
                <Label
                  for="basicInput"
                  className="text-uppercase letter-spacing"
                >
                 enter custom pip credits
                </Label>
                <Input
                  type="number"
                  id="basicInput"
                  placeholder="P500"
                  name="name"
                  value={handleInput}
                  onChange={(e) => sethandleInput(e.target.value)}
                />
                {/* <div className="validation">{errors?.name}</div> */}
              </FormGroup>
            </Col>
            <Col xl="12" className="my-2 mt-2" >
            {ispayment ? <Payment total={parseInt(handleInput)* 0.05 || parseInt(selectedVlaue) * 0.05} /> : 
            <Button  className="common_button" onClick={()=> setispayment(true)}>Buy Tokens</Button>}
          
          </Col>
    </Row>
    </CardBody>
    </Card>
    
    </>
}

export default BuyPIP
