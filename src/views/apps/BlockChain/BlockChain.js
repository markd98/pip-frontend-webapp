import React, { useEffect, useState } from 'react'
import { Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap'
import { Link, useHistory } from 'react-router-dom'

import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { selectThemeColors } from '@utils'
import Select from 'react-select'
import { Share2 } from 'react-feather'
import { ToastContainer, toast } from 'react-toastify';
import { Spin } from 'antd';
import 'antd/dist/antd.css';



import { ApiGet, ApiPost } from "../../../helpers/API/ApiData";

// const colourOptions = [
//   { value: 'India', label: 'India' },
//   { value: 'USA', label: 'USA' },
//   { value: 'Canada', label: 'Canada' }
// ]

const BlockChain = () => {
  const [centeredModal, setCenteredModal] = useState(false)
  const [List, setList] = useState([])
  const [committed, setCommitted] = useState([])
  const [unCommitted, setUnCommitted] = useState([])
  const [selectedIdea, setselectedIdea] = useState({})
  const [loader, setloader] = useState(false)
  const [balance, setbalance] = useState("")
  const [link, setlink] = useState("")

  const handleShare = () => {
    window.open(link)
  }

  const getIdea = async () => {
    let commit = []
    let uncommit = []
    await ApiGet(`/idea`)
      .then((res) => {
        // setList(res.data.data);
        res.data?.data?.map((item) => {
          if (item?.isBlockchainCommitted) {
            commit.push(item)
            // setCommitted((preValue) => ({
            //   ...preValue,
            //   [item]: item
            // }))
            setCommitted(commit)
          } else if (!item?.isBlockchainCommitted) {
            uncommit.push(item)
            // setUnCommitted((preValue) => ({
            //   ...preValue,
            //   [item]: item
            // }))
            setUnCommitted(uncommit)
          }
        })

        // setDetails(res?.data?.data);
      })
      .catch((err) => { });
  }

  useEffect(async () => {
    await getIdea()
    await ApiGet(``)
      .then((res) => {
        setbalance(res.data.data[0]);
        // setDetails(res?.data?.data);
      })
      .catch((err) => { });
  }, [])

  let colourOptions = List?.map((v) => {
    return { value: v._id, label: v.name };
  });

  const history = useHistory()

  const handleCommit = async (id) => {
    setloader(true)

    let body = {
      ideaId: id,
      PIPcredits: "P40"
    }
    await ApiPost(`/blockchain_commit`, body)
      .then(async (res) => {
        // commit = []
        // uncommit = []
        // toast.success( res.data.message)
        // await getIdea()
        setloader(false)
        setCenteredModal(!centeredModal)

        setlink(res?.data?.data?.explorer_url)
      })
      .catch(async (err) => {
        // await getIdea()
        setloader(false)
        // setCenteredModal(!centeredModal)

        toast.error(err.message)
      });


  }

  console.log("Commited", committed);
  console.log("unCommitted", unCommitted);
  return <>{loader ? <Spin>
    <div className='w-100'>

      <Col md={12} className='p-0'>
        <Card className=" w-100 scroller">
          <ToastContainer />
          <CardBody>
            <CardTitle tag="h2">Blockchain Commited</CardTitle>
            <hr />
            {
              committed?.length > 0 ?
                <Row>
                  <Col xl="12" className="mt-2">
                    <FormGroup className=" row align-items-center mx-0 ">

                      <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                        <b className="m-0 w-20 text-left">Name</b>
                        <b className="m-0 w-20 text-center">Number of commits</b>
                        <b className="m-0 w-20 text-center">PIP credits required</b>
                        <b className="m-0 w-20 text-center">PIP balance</b>
                        <b className="m-0 w-20 text-right">Action</b>
                      </div>
                    </FormGroup>

                  </Col>


                  {committed?.map((item, i) =>(
                   
                    <Col xl="12">
                      <FormGroup className=" row align-items-center mx-0 ">

                        <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                          <p className="m-0 w-20 text-left">{item.name}</p>
                          <p className="m-0 w-20 text-center">0</p>
                          <p className="m-0 w-20 text-center">P40</p>
                          <p className="m-0 w-20 text-center">P{balance?.accountBalance}</p>
                          <p className=' m-0 w-20 text-right'>
                            <span style={{ color: "#7367f0", cursor: "pointer", textAlign: "right" }}  onClick={()=> window.open(item?.commit[0]?.explorer_url)}> View on Solona </span>


                          </p>
                        </div>
                      </FormGroup>

                    </Col>

                  ))}
                  <Col xl="12">
                    {/* <FormGroup>
                <Label className="text-uppercase letter-spacing" for='register-username' >
                select pip to commit
                </Label>
                <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              defaultValue={colourOptions[1]}
              name='clear'
              options={colourOptions}
              onChange={(e)=> setselectedIdea(e)}
              isClearable
            />
              </FormGroup>
    </Col>
    <Col xl="12"  className="mt-2">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Number of commits for PIP </p>
                  <b className="m-0">0</b>
                </div>
              </FormGroup>
             
            </Col>
            <Col xl="12"  className="mt-2">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">PIP credits required</p>
                  <b className="m-0">P40</b>
                </div>
              </FormGroup>
             
            </Col><Col xl="12"  className="mt-2">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">PIP balance</p>
                  <b className="m-0">P{balance?.accountBalance}</b>
                </div>

               
              </FormGroup> */}

                    {/* <div onClick={() => history.push("/apps/buypip")} style={{ color: "#7367f0", cursor: "pointer", textAlign: "right" }}>
                  <Button className="common_button ">Buy PIP</Button>
                </div> */}


                  </Col>
                  {/* <Col xl="12"  className="mt-2">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 cursor-pointer" onClick={() => history.push("/apps/buypip")}  >
                  <p className="m-0">Purchase PIPs</p>
             
                </div>
              </FormGroup>
             
            </Col> */}
                  {/* <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={() => handleCommit()}> {loader ? "loading..." : "Commit"} </Button>
          </Col> */}
                </Row> :
                <CardTitle tag="h2" className='text-center'>There are no committed PiPs</CardTitle>
            }
          </CardBody>
        </Card>
      </Col>


      <Col md={12} className='p-0'>
        <Card className=" w-100 scroller">
          <ToastContainer />
          <CardBody>
            <CardTitle tag="h2">Commit to Blockchain</CardTitle>
            <hr />
            {
              unCommitted?.length ?
                <Row>
                  <Col xl="12" className="mt-2">
                    <FormGroup className=" row align-items-center mx-0 ">

                      <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                        <b className="m-0 w-20 text-left">Name</b>
                        <b className="m-0 w-20 text-center">Number of commits</b>
                        <b className="m-0 w-20 text-center">PIP credits required</b>
                        <b className="m-0 w-20 text-center">PIP balance</b>
                        <b className="m-0 w-20 text-right">Action</b>
                      </div>
                    </FormGroup>

                  </Col>


                  {unCommitted?.map((item, i) => (
                    <Col xl="12">
                      <FormGroup className=" row align-items-center mx-0 ">

                        <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                          <p className="m-0 w-20 text-left">{item.name}</p>
                          <p className="m-0 w-20 text-center">0</p>
                          <p className="m-0 w-20 text-center">P40</p>
                          <p className="m-0 w-20 text-center">P{balance?.accountBalance}</p>
                          <p className=' m-0 w-20 text-right'><span style={{ color: "#7367f0", cursor: "pointer", textAlign: "right" }} onClick={() => handleCommit(item?._id)}> Commit </span></p>
                        </div>
                      </FormGroup>

                    </Col>

                  ))}
                  {
                    unCommitted.length > 0 &&
                    <Col xl="12">
                      <div onClick={() => history.push("/apps/buypip")} style={{ color: "#7367f0", cursor: "pointer", textAlign: "right" }}>
                        <Button className="common_button ">Buy PIP</Button>
                      </div>
                    </Col>
                  }
                </Row> :
                <CardTitle tag="h2" className='text-center'>No PiPs found</CardTitle>
            }
          </CardBody>
        </Card>
      </Col>

    </div>
  </Spin> :
    <div className='w-100'>

      <Col md={12} className='p-0'>
        <Card className=" w-100 scroller">
          <ToastContainer />
          <CardBody>
            <CardTitle tag="h2">Blockchain Commited</CardTitle>
            <hr />
            {
              committed?.length > 0 ?
                <Row>
                  <Col xl="12" className="mt-2">
                    <FormGroup className=" row align-items-center mx-0 ">

                      <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                        <b className="m-0 w-20 text-left">Name</b>
                        <b className="m-0 w-20 text-center">Number of commits</b>
                        <b className="m-0 w-20 text-center">PIP credits required</b>
                        <b className="m-0 w-20 text-center">PIP balance</b>
                        <b className="m-0 w-20 text-right">Action</b>
                      </div>
                    </FormGroup>

                  </Col>


                  {committed?.map((item, i) => (
                    <Col xl="12">
                      <FormGroup className=" row align-items-center mx-0 ">

                        <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                          <p className="m-0 w-20 text-left">{item.name}</p>
                          <p className="m-0 w-20 text-center">0</p>
                          <p className="m-0 w-20 text-center">P40</p>
                          <p className="m-0 w-20 text-center">P{balance?.accountBalance}</p>
                          <p className=' m-0 w-20 text-right'>
                            <span style={{ color: "#7367f0", cursor: "pointer", textAlign: "right" }} onClick={()=> window.open(item?.commit[0]?.explorer_url)} > View on Solona </span>


                          </p>
                        </div>
                      </FormGroup>

                    </Col>

                  ))}
                  <Col xl="12">
                    {/* <FormGroup>
             <Label className="text-uppercase letter-spacing" for='register-username' >
             select pip to commit
             </Label>
             <Select
           theme={selectThemeColors}
           className='react-select'
           classNamePrefix='select'
           defaultValue={colourOptions[1]}
           name='clear'
           options={colourOptions}
           onChange={(e)=> setselectedIdea(e)}
           isClearable
         />
           </FormGroup>
 </Col>
 <Col xl="12"  className="mt-2">
           <FormGroup className=" row align-items-center mx-0 ">
           
             <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
               <p className="m-0">Number of commits for PIP </p>
               <b className="m-0">0</b>
             </div>
           </FormGroup>
          
         </Col>
         <Col xl="12"  className="mt-2">
           <FormGroup className=" row align-items-center mx-0 ">
           
             <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
               <p className="m-0">PIP credits required</p>
               <b className="m-0">P40</b>
             </div>
           </FormGroup>
          
         </Col><Col xl="12"  className="mt-2">
           <FormGroup className=" row align-items-center mx-0 ">
           
             <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
               <p className="m-0">PIP balance</p>
               <b className="m-0">P{balance?.accountBalance}</b>
             </div>

            
           </FormGroup> */}

                    {/* <div onClick={() => history.push("/apps/buypip")} style={{ color: "#7367f0", cursor: "pointer", textAlign: "right" }}>
               <Button className="common_button ">Buy PIP</Button>
             </div> */}


                  </Col>
                  {/* <Col xl="12"  className="mt-2">
           <FormGroup className=" row align-items-center mx-0 ">
           
             <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 cursor-pointer" onClick={() => history.push("/apps/buypip")}  >
               <p className="m-0">Purchase PIPs</p>
          
             </div>
           </FormGroup>
          
         </Col> */}
                  {/* <Col xl="12" className="my-2 mt-2" >
         <Button className="common_button" onClick={() => handleCommit()}> {loader ? "loading..." : "Commit"} </Button>
       </Col> */}
                </Row> :
                <CardTitle tag="h2" className='text-center'>There are no committed PiPs</CardTitle>
            }
          </CardBody>
        </Card>
      </Col>


      <Col md={12} className='p-0'>
        <Card className=" w-100 scroller">
          <ToastContainer />
          <CardBody>
            <CardTitle tag="h2">Commit to Blockchain</CardTitle>
            <hr />
            {
              unCommitted?.length ?
                <Row>
                  <Col xl="12" className="mt-2">
                    <FormGroup className=" row align-items-center mx-0 ">

                      <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                        <b className="m-0 w-20 text-left">Name</b>
                        <b className="m-0 w-20 text-center">Number of commits</b>
                        <b className="m-0 w-20 text-center">PIP credits required</b>
                        <b className="m-0 w-20 text-center">PIP balance</b>
                        <b className="m-0 w-20 text-right">Action</b>
                      </div>
                    </FormGroup>

                  </Col>


                  {unCommitted?.map((item, i) => (
                    <Col xl="12">
                      <FormGroup className=" row align-items-center mx-0 ">

                        <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                          <p className="m-0 w-20 text-left">{item.name}</p>
                          <p className="m-0 w-20 text-center">0</p>
                          <p className="m-0 w-20 text-center">P40</p>
                          <p className="m-0 w-20 text-center">P{balance?.accountBalance}</p>
                          <p className=' m-0 w-20 text-right'><span style={{ color: "#7367f0", cursor: "pointer", textAlign: "right" }} onClick={() => handleCommit(item?._id)}> Commit </span></p>
                        </div>
                      </FormGroup>

                    </Col>

                  ))}
                  {
                    unCommitted.length > 0 &&
                    <Col xl="12">
                      <div onClick={() => history.push("/apps/buypip")} style={{ color: "#7367f0", cursor: "pointer", textAlign: "right" }}>
                        <Button className="common_button ">Buy PIP</Button>
                      </div>
                    </Col>
                  }
                </Row> :
                <CardTitle tag="h2" className='text-center'>No PiPs found</CardTitle>
            }
          </CardBody>
        </Card>
      </Col>

    </div>
  }


    <Modal isOpen={centeredModal} toggle={() => setCenteredModal(!centeredModal)} className='modal-dialog-centered'>
      <ModalHeader>Success!</ModalHeader>
      <ModalBody className="text-center">
        <p>Congratulations!!!</p>
        <p>Your Idea is Commited in Solana Blockchain</p>
        <p>You Can view your NFT <span className='blue cursor-pointer' onClick={handleShare}>here</span></p>
        {/* <p>
           Your Genius idea is now being permanently recorded on a blockchain. All information is securely encrypted so that only can open and see PiP Idea
           </p>
           <p className='fw-600'>Wahoo! You are on your way to PiP Stardom!</p>
        
           <div className='d-flex justify-content-between align-items-center fw-600'>

<div>   <p>Share your PiP success with friends and Colleagues</p></div>
<div style={{padding: "6px",border: "1px solid #d8d6de",borderRadius: "0.357rem", cursor : "pointer", marginBottom : "14px"}} >

<Share2 size={19} />
</div>
</div>
           <p  className='fw-600'><span className='blue cursor-pointer'>Download </span> your PiP idea certificate </p>
           <p className='fw-600'>Plaque or <span  className='blue cursor-pointer' onClick={()=> history.push("/apps/services")}>Order an Heirloom Quality brass plaque </span>
 of your patent 
application.</p> */}
      </ModalBody>
      <ModalFooter>
        <Button color='primary' onClick={async () => {
          setCommitted([])
          setUnCommitted([])
          await getIdea()
          setCenteredModal(!centeredModal)
        }} className="common_button">
          Continue
        </Button>{' '}
      </ModalFooter>
    </Modal>
  </>
};

export default BlockChain;
