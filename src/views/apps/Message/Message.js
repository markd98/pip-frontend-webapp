import React,{useState} from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Alert } from 'reactstrap'
import { selectThemeColors } from '@utils'
import Select from 'react-select'
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "@styles/react/libs/editor/editor.scss";
import right from "../../../assets/images/myImg/right.png"

const colourOptions = [
  { value: 'India', label: 'India' },
  { value: 'USA', label: 'USA' },
  { value: 'Canada', label: 'Canada' }
]

const Message = () => {
  const [value, setValue] = useState(EditorState.createEmpty());
  const [centeredModal, setCenteredModal] = useState(false)

  return <><Card className=" w-100 scroller">
  <CardBody>
    <CardTitle tag="h2">PiP Message Center</CardTitle>
    <hr />
    <Row>
    <Col xl="12">
    <FormGroup>
                {/* <Label className="text-uppercase letter-spacing" for='register-username' >
                select pip to commit
                </Label> */}
                <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              defaultValue={colourOptions[1]}
              name='clear'
              options={colourOptions}
              isClearable
            />
                {/* {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null} */}
              </FormGroup>
      </Col>
      <Col xl="12"  className="mt-2">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">User 2 </p>
                  <p className="m-0">Join my team!</p>
                  <p className="m-0">8-10-2021</p>
                </div>
              </FormGroup>
             
            </Col>
            <Col xl="12"  className="mt-2">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                <p className="m-0">User 22 </p>
                  <p className="m-0">Offer for pip</p>
                  <p className="m-0">8-10-2021</p>
                </div>
              </FormGroup>
             
            </Col><Col xl="12"  className="mt-2">
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                <p className="m-0">PIP Admin </p>
                  <p className="m-0">pip Deposit!</p>
                  <p className="m-0">8-09-2021</p>
                </div>
              </FormGroup>
             
            </Col>
           
            <Col xl="12" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              send message
              </Label>
              <Input
                type="text"
                id="basicInput"
                placeholder="PIP User / Team"
              />
            </FormGroup>
          </Col>
          <Col xl="12" className="mt-2">
            <FormGroup>
              {/* <Label for="basicInput" className="text-uppercase letter-spacing">
                Explain your new idea
              </Label> */}
              <Editor
                editorState={value}
                onEditorStateChange={(data) => setValue(data)}
              placeholder="Description..."
              />
            </FormGroup>
          </Col>
            <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={() => setCenteredModal(!centeredModal)}>Send</Button>
          </Col>
      </Row>
      </CardBody>
      </Card>
      <Modal isOpen={centeredModal} toggle={() => setCenteredModal(!centeredModal)} className='modal-dialog-centered'>
          <ModalHeader toggle={() => setCenteredModal(!centeredModal)}>Message Sent</ModalHeader>
          <ModalBody className="text-center">
            <img src={right} alt="right" className="my-3"/>
           <p>Your message has been sent to
the PIP User/ Team.</p>
          </ModalBody>
          <ModalFooter>
            <Button color='primary' onClick={() => setCenteredModal(!centeredModal)} className="common_button">
              Done
            </Button>{' '}
          </ModalFooter>
        </Modal>
        </>
};

export default Message;
