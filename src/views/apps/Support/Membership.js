import React,{useState, useEffect} from "react";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { Link, useHistory } from 'react-router-dom'
import { ApiPost, ApiPostNoAuth, Bucket,ApiGet } from "../../../helpers/API/ApiData";
import axios from "axios"
import f from "../../../assets/images/myImg/1.png"
import s from "../../../assets/images/myImg/2.png"
import t from "../../../assets/images/myImg/3.png"
import l from "../../../assets/images/myImg/4.png"
import {XCircle  } from 'react-feather';
import Payment from "./Payment";
import { Handler } from "leaflet";
import { ToastContainer, toast } from 'react-toastify';



const Membership = (props) => {
    const [data, setdata] = useState([])
    const [total, settotal] = useState(0)
    const [num, setnum] = useState(1)
  const [ispayment, setispayment] = useState(false)

    useEffect(() => {
        setdata(props.location.state)
       
    let total1 = props.location.state.map(v => {
        settotal(total + v.fund.joiningPIPs)
      
    })

    }, [])

const removeData = (id) => {

  let dummy = data.filter(v =>{
     return v._id !== id
  })

  setdata(dummy)
  let total1 = dummy.map(v => {
    settotal(total + v.fund.joiningPIPs)
  
})
}


const paypalbtn  = () => {
  if(num >= (data[0]?.fund?.maxPercentageOffered)){
    toast.error("Please enter a valid value under the Max Percentage Offered.")
  }else if((100 - data[0]?.fund?.percentageOfCompletion) < data[0]?.fund?.maxPercentageOffered){
    if(num > (100 - data[0]?.fund?.percentageOfCompletion)){
      toast.error("Please enter a valid value under the percentage of Remiaining.")
    }
  }else{
    setispayment(true)
  }
}


  
    return (
        <Card className=" w-100 scroller">
        <CardBody>
        <CardTitle tag="h2">PIP Membership</CardTitle>
        <hr />
        <Row>
     <Col xl="12" className="mt-2">
            <FormGroup>
              <Label for="basicInput" className="text-uppercase letter-spacing">
              support PIPs Membership
              </Label>
              <br /> <br />
              <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <b className="m-0 w-16 text-left">Name</b>
                  <b className="m-0 w-16 text-center">joining PIPs</b>
                  <b className="m-0 w-16 text-center">max Percentage Offered</b>
                  <b className="m-0 w-16 text-center">percentage Of Remaining</b>
                  <b className="m-0 w-20 text-center"> Enter Number </b>
                  <b className="m-0 w-16 text-right">Contribution in Percentage</b>
                </div>
                {/* <XCircle onClick={() => removeData(v._id)}/> */}
              {data.map(v => {
                  return <>
                   <div className="border rounded row justify-content-between align-items-center m-0 px-1 py-50 flex-grow-1 mt-1">
                  <p className="m-0 w-16 text-left">{v?.name}</p>
                  <p className="m-0 w-16 text-center">P{v?.fund?.joiningPIPs}</p>
                  <p className="m-0 w-16 text-center">{v?.fund?.maxPercentageOffered}%</p>
                  <p className="m-0 w-16 text-center">{100 - v?.fund?.percentageOfCompletion}%</p>
                  <span className="m-0 w-16 text-center d-flex justify-content-center align-items-center"> <Input
                  type="number"
                  className="w-33 mr-1 text-center"
                  id="basicInput"
                  // placeholder="My First Great PIP Idea"
                  onChange={(e)=> setnum(e.target.value)}
                  min={1}
                  max={v?.fund?.maxPercentageOffered-1}
                  name="name"
                  value={num}
                />x &nbsp;{v?.fund?.percentagePerPIP}% &nbsp; =</span>
                  <p className="m-0 w-16 text-right">{num * v?.fund?.percentagePerPIP}%</p>
                </div>
          
                
                 <br/> 
                  </>
              })}
            
            </FormGroup>
            <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Total Support</p>
                  <b className="m-0">{parseInt(data[0]?.fund?.joiningPIPs) * parseInt(num)}</b>
                </div>
          </Col>

          <Col xl="12" className="mt-2 text-center">
                <img src={f}/>
                <img src={s} className="mx-3"/>
                <img src={t}/>
          </Col>
          <Col xl="12" className="mt-2 text-center">
          {ispayment ? <Payment items={data} num={num} total={parseInt(data[0]?.fund?.joiningPIPs) * parseInt(num)} checkid={data.map(v =>v._id)} /> : 
            <Button  className="common_button" onClick={()=> paypalbtn()}>Paypal</Button>}
          </Col>
                </Row>
                <Col xl="12" className="mt-2 text-center">
                    <p>Multiple Cryptocurrencies Accepted via</p>
                    <p className="fw-600">CoinPayments</p>
                </Col>

                <Col xl="12" className="mt-2 text-center">
                <img src={l}/>
              
          </Col>
       
       
      </CardBody>
      </Card>
    )
}

export default Membership
