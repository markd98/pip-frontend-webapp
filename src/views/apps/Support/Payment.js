import React,{useEffect, useRef} from 'react'
import { ApiPost, ApiPostNoAuth, Bucket,ApiGet } from "../../../helpers/API/ApiData";
import { Link, useHistory } from 'react-router-dom'


const Payment = ({items,checkid, total, num}) => {
    const paypal = useRef()
    const history = useHistory()

    useEffect(() => {
    //     window.paypal.Buttons({
    //     createOrder : (data,actions,err)=> {
    //         return actions.order.create({
    //             intent: 'CAPTURE',
    //             purchase_units : [
    //                 {
    //                     description : "PIP",
    //                     amount: {
    //                         currency_code : "USD",
    //                         value: 20.00,
    //                     },
    //                 }
    //             ]
    //         })
    //     },
    //     onApprove : async (data,actions) => {
    //         const order = await actions.order.capture()
    //     },
    //     onError : (ree) => {
    //     }
    //    }).render(paypal.current)

    let total1 = 0

    var price = items.map(v =>{ return total1= total1 + parseInt(v?.fund?.joiningPIPs) * 0.05})

    // function add(accumulator, a) {
    //     return accumulator + a;
    //   }
      



    window.paypal.Buttons({
        style: {
            layout:  'vertical',
            color:   'blue',
            shape:   'rect',
            label:   'paypal',
            height : 36,
          },
        
        createOrder: function(data, actions) {
            
          // This function sets up the details of the transaction, including the amount and line item details.
          return actions.order.create({
            // style: {
            //     layout:  'horizontal',
            //     color:   'red',
            //     shape:   'rect',
            //     label:   'paypal'
            //   },
           
            purchase_units: [{
            //  item_list : items,
              amount: {
                value:  total * 0.05
                // parseInt(price*10)*0.1
              }
            }]
          });
        },
        onApprove : async (data,actions) => {
            const order = await actions.order.capture()
            let body = {
                pipId:checkid,
                amount:total * 0.05,
                paymentURL:order?.links[0]?.href,
                paymentType:1,
                itemList:items,
                enteredInput : [num]
            }
             await ApiPost("/paypal/payment_check", body)
              .then((res) => {
                // getServices()
                setTimeout(() => {
                  
                  history.push("support")
                }, 2000);
              //   window.location.href = res?.data?.forwardLink
              })
              .catch((err) => {});

        },
        onError : (ree) => {
        }
        
      }).render(paypal.current);
    }, [])
    
    return (
        <div>
           <div ref={paypal}></div>
        </div>
    )
}

export default Payment
