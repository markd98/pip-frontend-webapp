import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { Link, useHistory } from "react-router-dom";
import { Alert } from "reactstrap";
import {
  ApiPost,
  ApiPostNoAuth,
  Bucket,
  ApiGet,
} from "../../../helpers/API/ApiData";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";

// import Payment from "./Payment";

const PipSupport = () => {
  const [supportList, setsupportList] = useState([]);
  const [notSupportList, setnotSupportList] = useState([]);
  const [items, setitems] = useState([]);
  const [visible, setvisible] = useState(true);

  const history = useHistory();

  const notSupport = async () => {
    await ApiPost("/get_support_idea", {
      page: 1,
      limit: 10,
      isSupported: false,
    })
      .then((res) => {
        setnotSupportList(res?.data?.data?.idea_data?.idea);
        // toast.success( res.data.message)
        //   setunPurchasedService(res?.data?.data?.unPurchasedService)
        //   setpurchasedService(res?.data?.data?.purchasedService)
      })
      .catch((err) => {
        // toast.error( err.message)
      });
  };

  const support = async () => {
    await ApiPost("/get_support_idea", {
      page: 1,
      limit: 10,
      isSupported: true,
    })
      .then((res) => {
        setsupportList(res?.data?.data?.idea_data?.idea);
        // toast.success( res.data.message)
        //   setunPurchasedService(res?.data?.data?.unPurchasedService)
        //   setpurchasedService(res?.data?.data?.purchasedService)
      })
      .catch((err) => {
        // toast.error( err.message)
      });
  };
  useEffect(() => {
    notSupport();
    support();
  }, []);

  const checkedValue = (e, item) => {
    // if( e.target.checked){
    setitems([item]);
  };

  const validateForm = () => {
    let formIsValid = true;

    if (items.length <= 0) {
      formIsValid = false;
      toast.error("Atleast Select One PiP Item");
    }

    // setError(errors);
    return formIsValid;
  };

  const routing = () => {
    if (validateForm()) {
      history.push({
        pathname: "/apps/membership",
        state: items,
      });
    }
  };

  const onDismiss = () => {
    setvisible(false);
  };

  return (
    <>
      <ToastContainer />
      <Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">Support a PIP</CardTitle>
          <hr />
          <Row>
            {notSupportList?.map((item, i) => (
              <Col xl="12">
                <FormGroup className=" row align-items-center mx-0 ">
                  <CustomInput
                    type="radio"
                    id={i}
                    name="customRadio"
                    inline
                    label=""
                    onChange={(e) => checkedValue(e, item)}
                    // defaultChecked
                  />
                  <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                    <p className="m-0 w-33 text-left">{item.name}</p>
                    <p className="m-0 w-33 text-center">
                      P{item?.fund?.joiningPIPs}/{item.fund.percentagePerPIP}%
                    </p>
                    <b className="m-0 w-33 text-right">
                      {" "}
                      {100 - item.fund.percentageOfCompletion}% remains{" "}
                    </b>
                  </div>
                </FormGroup>
              </Col>
            ))}
            {/* <Col xl="12" className="my-2"> */}
            {/* <Payment items={items} /> */}
            {/* {supportList ? <Payment items={items} checkid={checkid} getServices={getServices}/> : <Button className="common_button" onClick={()=> payment()}>Payment</Button>} */}
            {/* <button>Support PIPs</button> */}
            {/* <Button className="common_button" onClick={()=> payment()}>Payment</Button> */}

            {/* </Col> */}
          </Row>
          <Row>
            {supportList?.map((item, i) => (
              <Col xl="12">
                <FormGroup className=" row align-items-center mx-0 ">
                  {/* <CustomInput
                  type="radio"
                  id={i}
                  name="customRadio"
                  inline
                  label=""
                //   onChange={(e)=> checkedValue(e,item)}
                  // defaultChecked
                /> */}
                  <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                    <p className="m-0 w-33 text-left">{item.name}</p>
                    <p className="m-0 w-33 text-center">
                      P{item?.fund?.joiningPIPs}/{item?.fund?.percentagePerPIP}%
                    </p>
                    <b className="m-0 w-33 text-right">
                      {100 - item.fund?.percentageOfCompletion}% remains{" "}
                    </b>
                  </div>
                </FormGroup>
              </Col>
            ))}
            <Col xl="12" className="">
              {/* <Payment items={items} /> */}
              <CardText tag="p" className="mt-2">
                <span className="fw-600 ">Terms and Conditions:</span> Nulla
                wisi laoreet suspendisse integer vivamus elit eu mauris
                hendrerit facilisi, mi mattis pariatur aliquam pharetra eget.
              </CardText>

              <Alert
                color="danger"
                isOpen={visible}
                toggle={onDismiss}
                style={{ padding: "10px" }}
              >
                Users assume all responsibility for any transactions,
                contributions, investments, support funds, or exchanges you
                engage in on the PiPidea.com platform. PiPidea.com cannot be
                held liable.
              </Alert>
              {/* {supportList ? <Payment items={items} checkid={checkid} getServices={getServices}/> : <Button className="common_button" onClick={()=> payment()}>Payment</Button>} */}
              <Button className="common_button my-2" onClick={() => routing()}>
                Support PIPs
              </Button>
              {/* <Button className="common_button" onClick={()=> payment()}>Payment</Button> */}
            </Col>
          </Row>
        </CardBody>
      </Card>
    </>
  );
};

export default PipSupport;
