import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { Link, useHistory } from 'react-router-dom'
import sort from "../../../assets/images/myImg/sort.png"
const data = [
  {
    id: 1,
    name: "Patent Search",
    pip: "2",
    price : "400",
  },
  {
    id: 2,
    name: "PIP Plaque",
    pip: "2",
    price : "120",
  },
 
 
];

const PurchaseDetails = () => {p
  const history = useHistory()
  return <><Card className=" w-100 scroller">
  <CardBody>
    <CardTitle tag="h2">Purchase Details</CardTitle>
    <hr />
    <Row>
    
    <Col xl="12" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <b className="m-0">Items</b>
                  <b className="m-0">QTY</b>
                  <b className="m-0">PIPs</b>
                </div>
              </FormGroup>
             
            </Col> 
            <div className="w-100">
            {data.map((item, i) => (
            <Col xl="12">
              <FormGroup className=" row align-items-center mx-0">
                
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 cursor-pointer" onClick={()=> history.push("/apps/pip-idea")}>
                  <p className="m-0 w-33 text-left" >{item.name}</p>
                  <p className="m-0 w-33 text-center">{item.pip}</p>
                  <p className="m-0 w-33 text-right">{item.price}</p>
                </div>
              </FormGroup>
             
            </Col>
            
          ))}
          </div>

<Col xl="12 mt-2" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">Total</p>
                  <b className="m-0">520</b>
                </div>
              </FormGroup>
             
            </Col>   <Col xl="12" >
              <FormGroup className=" row align-items-center mx-0 ">
              
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1 cursor-pointer" onClick={() => history.push("/apps/services")}>
                  <p className="m-0">Account Balance</p>
                  <b className="m-0">10000</b>
                </div>
              </FormGroup>
             
            </Col> 
            <Col xl="12" className="my-2 mt-2" >
            <Button className="common_button" onClick={() => history.push("/apps/newIdea")}>Home</Button>
          </Col>
            </Row>
            </CardBody>
            </Card>
            </>
};

export default PurchaseDetails;


