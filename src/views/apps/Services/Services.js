import React,{useState, useEffect} from "react";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { Link, useHistory } from 'react-router-dom'
import { ApiPost, ApiPostNoAuth, Bucket,ApiGet } from "../../../helpers/API/ApiData";
import axios from "axios"
import Payment from "./Payment";


const Services = () => {
  const [unPurchasedService, setunPurchasedService] = useState([])
  const [purchasedService, setpurchasedService] = useState([])
  const [checkid, setcheckid] = useState([])
  const [ispayment, setispayment] = useState(false)
  const [items, setitems] = useState([])
  const history = useHistory()


  const getServices = async () => {
    await ApiGet("/service")
    .then((res) => {
      setunPurchasedService(res?.data?.data?.unPurchasedService)
      setpurchasedService(res?.data?.data?.purchasedService)
    })
    .catch((err) => {});
  }
  useEffect(() => {
    getServices()
  }, [])


  const payment =async () => {
    // await axios.get("https://api.homiesbusiness.com/v1/paypal/pay?price=200&userId=61b81b3b4390953c3bc24e7c").then((res) => {
    // })
    // .catch((err) => {});
    // await ApiPost("/paypal_initiate", {serviceId : checkid})
    // .then((res) => {
    //   window.location.href = res?.data?.forwardLink
    // })
    // .catch((err) => {});
    setispayment(true)
    
    // history.push("/apps/purchase-details")
  }

  // let data1 = []
  const checkedValue = (e,item) => {
     
      if( e.target.checked){
        setcheckid([...checkid,item._id])
        setitems([...items, {name: item?.name,
          price: (parseInt(item?.price) * 0.1),
          _id : item._id
         }])
        
     
      }else{
      const datta = checkid.filter(v =>{
        return v !== item._id
      })
      setcheckid(datta)
      
      const datta2 = unPurchasedService.filter(v=>{
        if(datta.includes(v._id)){
        return v
        }
      })

      let final = datta2.map(item =>{
        return  {name: item?.name,
          price: (parseInt(item?.price) * 0.1),
          _id : item._id}
      })

      setitems(final)
      }
        
    }
    let total = 0
    var price = items.map(v =>{ return total = parseInt(total) + parseInt(v.price)})

  
  return (
    <Card className=" w-100 scroller">
      <CardBody>
        <CardTitle tag="h2">PiP Services</CardTitle>
        <hr />
        <CardText tag="p">Purchased Services</CardText>
        <Row>
          {purchasedService?.map((item, i) => (
            <Col xl="12">
              <FormGroup className=" row align-items-center mx-0 ">
                {/* <CustomInput
                  type="checkbox"
                  id={i}
                  name="customRadio"
                  inline
                  label=""
                  onChange={(e)=> checkedValue(e,item)}
                  // defaultChecked
                /> */}
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">{item.name}</p>
                  <b className="m-0">{item.price} PIP</b>
                </div>
              </FormGroup>
             
            </Col>
            
          ))}
          {/* <div className="d-flex justify-content-between w-100 card-body " style={{paddingLeft : "5%"}}> 
               <h5 className="fw-600">Total Services </h5> 
               <h5 className="blue fw-600"> 1500 Pips</h5>
             </div>

          <Col xl="12" className="my-2">
            <Button className="common_button" onClick={()=> payment()}>Payment</Button>
          </Col> */}
        </Row>
      { unPurchasedService.length > 0 &&  <CardText tag="p">Select all the services you would like</CardText>}
        <Row>
          {unPurchasedService?.map((item, i) => (
            <Col xl="12">
              <FormGroup className=" row align-items-center mx-0 ">
                <CustomInput
                  type="checkbox"
                  id={i}
                  name="customRadio"
                  inline
                  label=""
                  onChange={(e)=> checkedValue(e,item)}
                  // defaultChecked
                />
                <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                  <p className="m-0">{item.name}</p>
                  <b className="m-0">{item.price} PIP</b>
                </div>
              </FormGroup>
             
            </Col>
            
          ))}
        { unPurchasedService.length > 0 &&  <div className="d-flex justify-content-between w-100 card-body " style={{paddingLeft : "5%"}}> 
               <h5 className="fw-600">Total Services </h5> 
               <h5 className="blue fw-600"> {parseInt(total) * 10} Pips</h5>
             </div>}

          <Col xl="12" className="my-2">
          {/* <Payment items={items} /> */}
            {ispayment ? <Payment items={items} checkid={checkid} getServices={getServices}/> : 
           unPurchasedService.length > 0 && <Button className="common_button" onClick={()=> history.push({pathname : "/apps/pay", state : {items: items,  checkid:checkid}})}>Payment</Button>
             } 
            {/* <Button className="common_button" onClick={()=> payment()}>Payment</Button> */}

          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};

export default Services;
