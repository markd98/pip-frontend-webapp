import React,{useState, useEffect, useRef} from "react";
import {
  Button,
  Card,
  CardBody,
  CardTitle,
  Col,
  CustomInput,
  FormGroup,
  Input,
  CardText,
  Label,
  Row,
} from "reactstrap";
import { Link, useHistory } from 'react-router-dom'
import { ApiPost, ApiPostNoAuth, Bucket,ApiGet } from "../../../helpers/API/ApiData";


const Payment = (props) => {
  const [items, setitems] = useState([])
  const [checkid, setcheckid] = useState([])
  const [finalTotal, setfinalTotal] = useState(0)
    const paypal = useRef()
    const history = useHistory()

    useEffect(() => {
      setitems(props?.location?.state?.items)
      setcheckid(props?.location?.state?.checkid)
   
    let total = 0
    var price = props?.location?.state?.items?.map(v =>{ return total = total + parseInt(v.price) * 0.5})

    let grandTotal = 0

    let sum = props?.location?.state?.items?.map(v =>{ return grandTotal = grandTotal + parseInt(v.price) })
    setfinalTotal(grandTotal)

    window.paypal.Buttons({
        style: {
            layout:  'vertical',
            color:   'blue',
            shape:   'rect',
            label:   'paypal',
            height : 36,
          },
        
        createOrder: function(data, actions) {
            
          // This function sets up the details of the transaction, including the amount and line item details.
          return actions.order.create({
            // style: {
            //     layout:  'horizontal',
            //     color:   'red',
            //     shape:   'rect',
            //     label:   'paypal'
            //   },
           
            purchase_units: [{
            //  item_list : items,
              amount: {
                value:  total
                // parseInt(price*10)*0.1
              }
            }]
          });
        },
        onApprove : async (data,actions) => {
            const order = await actions.order.capture()
            let body = {
                serviceId:props?.location?.state?.checkid,
                amount:total,
                paymentURL:order?.links[0]?.href,
                paymentType:0,
                itemList:items
            }
             await ApiPost("/paypal/payment_check", body)
              .then((res) => {
                // getServices()
                setTimeout(() => {
                  
                  history.goBack()
                }, 1000);
              //   window.location.href = res?.data?.forwardLink
              })
              .catch((err) => {});

        },
        onError : (ree) => {
        }
        
      }).render(paypal.current);
    }, [])
    
    return (
        // <div>
        //    <div ref={paypal}></div>
        // </div>
        <Card className=" w-100 scroller">
        <CardBody>
          <CardTitle tag="h2">Payment</CardTitle>
          <hr />
        
          <Row>
          {items?.map((v, i) => (
              <Col xl="12">
                <FormGroup className=" row align-items-center mx-0 ">
                 
                  <div className="border rounded row justify-content-between m-0 px-1 py-50 flex-grow-1">
                    <p className="m-0">{v.name}</p>
                    <b className="m-0">P{v.price * 10}</b>
                  </div>
                </FormGroup>
               
              </Col>
              
              ))}
          
          </Row>
          <Row>
    
         
  
            <Col xl="12" className="my-2">
            <div className="border rounded row justify-content-between m-0 px-1 py-50 mb-5 flex-grow-1">
                    <p className="m-0">Total</p>
                    <b className="m-0">P{finalTotal * 10} ${finalTotal * 0.5}</b>
                  </div>
            {/* <Payment items={items} /> */}
           
              {/* <Button className="common_button" onClick={()=> history.goBack()}>Payment</Button> */}
              <div ref={paypal}></div>
             
              {/* <Button className="common_button" onClick={()=> payment()}>Payment</Button> */}
  
            </Col>
          </Row>
        </CardBody>
      </Card>
    )
}

export default Payment
