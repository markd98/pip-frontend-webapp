import { Toast, ToastBody, ToastHeader, Spinner, Row, Col } from 'reactstrap'
import logo1 from "../../../assets/images/myImg/PiPlogo.png"


const logo = (
  <img src={logo1} alt="pip" className='img-fluid' width={"250px"} />
)

const close = (
  <button type='button' className='ml-1 close'>
    <span>×</span>
  </button>
)

const ToastHeaderIcons = () => {
  return (
    <Row className='demo-vertical-spacing'>
      <Col md='6' sm='12'>
        <Toast>
          <ToastHeader close={close} icon='primary'>
            Vuexy
          </ToastHeader>
          <ToastBody>This is a toast with a primary icon — check it out!</ToastBody>
        </Toast>
      </Col>
      <Col md='6' sm='12'>
        <Toast>
          <ToastHeader close={close} icon='success'>
            Vuexy
          </ToastHeader>
          <ToastBody>This is a toast with a success icon — check it out!</ToastBody>
        </Toast>
      </Col>
      <Col md='6' sm='12'>
        <Toast>
          <ToastHeader close={close} icon='info'>
            Vuexy
          </ToastHeader>
          <ToastBody>This is a toast with a info icon — check it out!</ToastBody>
        </Toast>
      </Col>
      <Col md='6' sm='12'>
        <Toast>
          <ToastHeader close={close} icon='danger'>
            Vuexy
          </ToastHeader>
          <ToastBody>This is a toast with a danger icon — check it out!</ToastBody>
        </Toast>
      </Col>
      <Col md='6' sm='12'>
        <Toast>
          <ToastHeader close={close} icon='warning'>
            Vuexy
          </ToastHeader>
          <ToastBody>This is a toast with a warning icon — check it out!</ToastBody>
        </Toast>
      </Col>
      <Col md='6' sm='12'>
        <Toast>
          <ToastHeader close={close} icon='dark'>
            Vuexy
          </ToastHeader>
          <ToastBody>This is a toast with a dark icon — check it out!</ToastBody>
        </Toast>
      </Col>

      <Col md='6' sm='12'>
        <Toast>
          <ToastHeader close={close} icon={<Spinner size='sm' color='primary' />}>
            Vuexy
          </ToastHeader>
          <ToastBody>This is a toast with a spinner — check it out!</ToastBody>
        </Toast>
      </Col>

      <Col md='6' sm='12'>
        <Toast>
          <ToastHeader close={close} icon={logo}>
            Vuexy
          </ToastHeader>
          <ToastBody>This is a toast with a logo — check it out!</ToastBody>
        </Toast>
      </Col>
    </Row>
  )
}
export default ToastHeaderIcons
