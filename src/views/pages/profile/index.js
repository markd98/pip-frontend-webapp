import { Fragment, useState, useEffect } from "react";

import { selectThemeColors } from "@utils";

import {
  Row,
  Col,
  Button,
  Card,
  Label,
  CardBody,
  CardTitle,
  FormGroup,
  CustomInput,
  Input,
} from "reactstrap";
import Select from "react-select";

import "@styles/react/pages/page-profile.scss";
import { ApiGet, ApiGetNoAuth, Bucket } from "../../../helpers/API/ApiData";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { useHistory } from "react-router-dom";

const Profile = () => {
  const [countryList, setcountryList] = useState([]);
  const [profileData, setprofileData] = useState({});

  const history = useHistory();

  const getCountry = async () => {
    await ApiGetNoAuth("country")
      .then((res) => {
        setcountryList(res?.data?.data);
      })
      .catch((err) => {});
  };
  useEffect(async () => {
    await ApiGet("").then((response) =>
      setprofileData(
        response?.data?.data?.length > 0 && response?.data?.data?.[0]
      )
    );
    getCountry();
  }, []);
  console.log("profileData", profileData);

  let colourOptions = countryList?.map((v) => {
    return { value: v.country, label: v.country };
  });

  const handleCountry = (e) => {
    setprofileData({
      ...profileData,
      countryName: e.value,
    });
  };

  const handleNumber = (phone, e) => {
    console.log("🚀 ~ file: index.js ~ line 77 ~ handleNumber ~ phone", phone);
    let phone1 = phone;
    let countryCode = e.dialCode;
    let phoneNumber = phone1.split(countryCode)[1];
    setprofileData({
      ...profileData,
      countryCode,
      phoneNumber,
    });
  };
  return (
    <Fragment>
      <div id="user-profile">
        <Card className=" w-100 scroller">
          <CardBody>
            <CardTitle tag="h2">Profile</CardTitle>
            <hr />
            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup className="text-center">
                  <img
                    src={
                      profileData?.image
                        ? Bucket + profileData?.image
                        : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJxA5cTf-5dh5Eusm0puHbvAhOrCRPtckzjA&usqp=CAU"
                    }
                    height={175}
                    width={170}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup>
                  <Label
                    for="basicInput"
                    className="text-uppercase letter-spacing"
                  >
                    Email
                  </Label>
                  <Input
                    type="email"
                    value={profileData?.email}
                    id="register-email1"
                    name="email"
                    disabled
                    // onChange={handleChange}
                    placeholder="john@example.com"
                  />
                  {/* <div className="validation">{errors?.name}</div> */}
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup>
                  <Label
                    for="basicInput"
                    className="text-uppercase letter-spacing"
                  >
                    Username
                  </Label>
                  <Input
                    type="text"
                    autoComplete="nope"
                    value={profileData?.username}
                    placeholder="johndoe"
                    disabled
                    // id='username'
                    name="username"
                    // onChange={handleChange}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup>
                  <Label
                    for="basicInput"
                    className="text-uppercase letter-spacing"
                  >
                    Country
                  </Label>
                  <Input
                    type="text"
                    autoComplete="nope"
                    value={profileData?.countryName}
                    placeholder="johndoe"
                    disabled
                    // id='username'
                    name="username"
                    // onChange={handleChange}
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup>
                  <Label
                    for="basicInput"
                    className="text-uppercase letter-spacing"
                  >
                    Mobile Number for 2FA
                  </Label>
                  <Input
                    type="text"
                    autoComplete="nope"
                    value={
                      "+" +
                      profileData?.countryCode +
                      " " +
                      profileData?.phoneNumber
                    }
                    placeholder="johndoe"
                    disabled
                    // id='username'
                    name="username"
                    // onChange={handleChange}
                  />
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <button
                type="button"
                class="common_button my-2 py-1 mx-1 btn btn-secondary"
                onClick={() => history.push("/pages/edit-profile")}
              >
                Edit Profile
              </button>
            </Row>
          </CardBody>
        </Card>
      </div>
    </Fragment>
  );
};

export default Profile;
