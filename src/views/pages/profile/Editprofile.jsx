import { Fragment, useState, useEffect } from "react";

import { selectThemeColors } from "@utils";

import {
  Row,
  Col,
  Button,
  Card,
  Label,
  CardBody,
  CardTitle,
  FormGroup,
  CustomInput,
  Input,
} from "reactstrap";
import Select from "react-select";

import "@styles/react/pages/page-profile.scss";
import {
  ApiGet,
  ApiGetNoAuth,
  ApiPut,
  ApiUpload,
  Bucket,
} from "../../../helpers/API/ApiData";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { useHistory } from "react-router-dom";
import { FiCamera } from "react-icons/fi";
import { AiFillCamera } from "react-icons/ai";

const Editprofile = () => {
  const [countryList, setcountryList] = useState([]);
  const [profileData, setprofileData] = useState({});
  const [profileImage, setprofileImage] = useState([]);
  console.log("profileImage", profileImage);

  const history = useHistory();

  const getCountry = async () => {
    await ApiGetNoAuth("country")
      .then((res) => {
        setcountryList(res?.data?.data);
      })
      .catch((err) => {});
  };
  useEffect(async () => {
    await ApiGet("").then((response) =>
      setprofileData(
        response?.data?.data?.length > 0 && response?.data?.data?.[0]
      )
    );
    getCountry();
  }, []);
  console.log("profileData", profileData);

  let colourOptions = countryList?.map((v) => {
    return { value: v.country, label: v.country };
  });

  const handleCountry = (e) => {
    setprofileData({
      ...profileData,
      countryName: e.value,
    });
  };

  const handleNumber = (phone, e) => {
    console.log("🚀 ~ file: index.js ~ line 77 ~ handleNumber ~ phone", phone);
    let phone1 = phone;
    let countryCode = e.dialCode;
    let phoneNumber = phone1.split(countryCode)[1];
    setprofileData({
      ...profileData,
      countryCode,
      phoneNumber,
    });
  };

  const saveProfile = async () => {
    const compressedImage = await uploadImage(profileImage);
    console.log("compressedImage", compressedImage);
    const body = {
      username: profileData?.username,
      countryCode: profileData?.countryCode,
      phoneNumber: profileData?.phoneNumber,
      countryName: profileData?.countryName,
      image: compressedImage,
    };

    await ApiPut("", body).then((res) => {
      //   history.push("/pages/profile");
      window.location.pathname = "/pages/profile";
    });
    console.log("body", body);
  };
  const handleimage = (e) => {
    let file = e.target.files[0];
    let fileURL = URL.createObjectURL(file);
    file.fileURL = fileURL;
    // }
    setprofileImage([file]);
  };

  const uploadImage = async (v) => {
    const formData = new FormData();
    let thub;
    if (v[0]?.fileURL) {
      formData.append("image", v[0]);
      await ApiUpload("upload/compress_image/profile", formData, {})
        .then((res) => {
          thub = res?.data?.data?.image;
        })
        .catch((err) => {
          //   ErrorToast(err?.message);
        });
    } else {
      thub = v[0];
    }
    return thub;
  };
  return (
    <Fragment>
      <div id="user-profile">
        <Card className=" w-100 scroller">
          <CardBody>
            <CardTitle tag="h2">Edit Profile</CardTitle>
            <hr />
            <Row>
              <Col xl="12" className="mt-2 d-flex justify-content-center">
                <FormGroup
                  className="text-center position-relative"
                  style={{ height: "170px", width: "175px" }}
                >
                  {/* <input type="file"></input> */}
                  <img
                    // src={
                    //   Bucket + profileData?.image ||
                    //   "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJxA5cTf-5dh5Eusm0puHbvAhOrCRPtckzjA&usqp=CAU"
                    // }
                    src={
                      profileImage[0]?.fileURL
                        ? profileImage[0]?.fileURL
                        : profileData?.image
                        ? Bucket + profileData?.image
                        : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJxA5cTf-5dh5Eusm0puHbvAhOrCRPtckzjA&usqp=CAU"
                    }
                    height={175}
                    width={170}
                  />
                  <div className="position-absolute labelFile">
                    <label htmlFor="images" className="profilee">
                      <AiFillCamera size={25} color="white" />
                      <input
                        type="file"
                        id="images"
                        name="profile_avatar"
                        accept=".png, .jpg, .jpeg"
                        hidden
                        onChange={(e) => handleimage(e)}
                      />
                    </label>
                  </div>
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup>
                  <Label
                    for="basicInput"
                    className="text-uppercase letter-spacing"
                  >
                    Email
                  </Label>
                  <Input
                    type="email"
                    value={profileData?.email}
                    id="register-email1"
                    name="email"
                    disabled
                    // onChange={handleChange}
                    placeholder="john@example.com"
                  />
                  {/* <div className="validation">{errors?.name}</div> */}
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup>
                  <Label
                    for="basicInput"
                    className="text-uppercase letter-spacing"
                  >
                    Username
                  </Label>
                  <Input
                    type="text"
                    autoComplete="nope"
                    value={profileData?.username}
                    placeholder="johndoe"
                    // id='username'
                    name="username"
                    onChange={(e) =>
                      setprofileData({
                        ...profileData,
                        username: e.target.value,
                      })
                    }
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup>
                  <Label
                    for="basicInput"
                    className="text-uppercase letter-spacing"
                  >
                    Country
                  </Label>
                  <Select
                    theme={selectThemeColors}
                    className="react-select"
                    classNamePrefix="select"
                    onChange={handleCountry}
                    value={{
                      label: profileData?.countryName,
                      value: profileData?.countryName,
                    }}
                    name="countryName"
                    options={colourOptions}
                    isClearable
                  />
                </FormGroup>
              </Col>
            </Row>

            <Row>
              <Col xl="12" className="mt-2">
                <FormGroup>
                  <Label
                    for="basicInput"
                    className="text-uppercase letter-spacing"
                  >
                    Mobile Number for 2FA
                  </Label>
                  <PhoneInput
                    inputStyle={{ width: "100%", height: "38px" }}
                    country={"us"}
                    specialLabel={""}
                    // autoFormat={true}
                    value={profileData?.countryCode + profileData?.phoneNumber}
                    onChange={(phone, e) => handleNumber(phone, e)}
                  />
                  {/* <div className="validation">{errors?.phoneNumber}</div> */}
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <button
                type="button"
                class="common_button my-2 py-1 mx-1 btn btn-secondary"
                onClick={saveProfile}
              >
                Save
              </button>
            </Row>
          </CardBody>
        </Card>
      </div>
    </Fragment>
  );
};

export default Editprofile;
