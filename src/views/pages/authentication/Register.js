import { Fragment, useState, useContext, useEffect } from "react";
import { useSkin } from "@hooks/useSkin";
import { Link, useHistory } from "react-router-dom";
import InputPasswordToggle from "@components/input-password-toggle";
import OtpInput from "react-otp-input";
import {
  Row,
  Col,
  CardTitle,
  CardText,
  FormGroup,
  Label,
  Button,
  Form,
  Input,
  CustomInput,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import logo from "../../../assets/images/myImg/PiPlogo.png";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import "@styles/base/pages/page-auth.scss";
import Select from "react-select";
import { selectThemeColors } from "@utils";
import { Share2 } from "react-feather";
import right from "../../../assets/images/myImg/right.png";
import { ToastContainer, toast } from "react-toastify";
import validator from "validator";

import {
  ApiPost,
  ApiPostNoAuth,
  Bucket,
  ApiGetNoAuth,
} from "../../../helpers/API/ApiData";
const otpInput = {
  width: "100%",
  padding: "15px 0",
  borderRadius: "5px",
  border: "none",
  margin: "0 5px",
  backgroundColor: "#f3f6f9",
  borderColor: "#F3F6F9",
  fontSize: "18px",
  outline: "none",
};

const Register = () => {
  const [skin, setSkin] = useSkin();

  const history = useHistory();

  const [formModal, setFormModal] = useState(false);
  const [centeredModal, setCenteredModal] = useState(false);
  const [centeredModal1, setCenteredModal1] = useState(false);
  const [countryList, setcountryList] = useState([]);
  const [signUpData, setsignUpData] = useState({ teamMember: [], email: "" });
  const [errors, setError] = useState({});
  const [errors2, setError2] = useState({});
  const [otp, setotp] = useState("");
  const [otpValidation, setotpValidation] = useState("");

  const illustration =
      skin === "dark" ? "register-v2-dark.svg" : "register-v2.svg",
    source = require(`@src/assets/images/pages/${illustration}`).default;

  const getCountry = () => {
    ApiGetNoAuth("country")
      .then((res) => {
        setcountryList(res?.data?.data);
      })
      .catch((err) => {});
  };
  useEffect(() => {
    getCountry();
  }, []);

  let colourOptions = countryList?.map((v) => {
    return { value: v.country, label: v.country };
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setsignUpData({
      ...signUpData,
      [name]: value,
    });
  };

  const handleCountry = (e) => {
    setsignUpData({
      ...signUpData,
      countryName: e.value,
    });
  };

  const validateForm = () => {
    let errors = {};
    let formIsValid = true;

    if (signUpData.email === "") {
      formIsValid = false;
      errors["email"] = "Email is requried";
    }
    if (!validator.isEmail(signUpData.email)) {
      formIsValid = false;
      errors["email"] = "Enter Valid Email";
    }
    if (!signUpData.password) {
      formIsValid = false;
      errors["password"] = "Password is Requried";
    }
    if (!signUpData.username) {
      formIsValid = false;
      errors["username"] = "Username is Requried";
    }
    if (!signUpData.countryName) {
      formIsValid = false;
      errors["country"] = "Country is Requried";
    }
    if (!signUpData.username) {
      formIsValid = false;
      errors["phoneNumber"] = "Phone Number is Requried";
    }
    // if (!signUpData.teamName) {
    //   formIsValid = false;
    //   errors["teamName"] = "teamName is Requried";
    // }
    //  if (!signUpData.companyName) {
    //   formIsValid = false;
    //   errors["companyName"] = "companyName is Requried";
    // }
    if (!signUpData.username) {
      formIsValid = false;
      errors["username"] = "Username is Requried";
    }

    setError(errors);
    return formIsValid;
  };

  const handleSubmit = async () => {
    if (validateForm()) {
      await ApiPostNoAuth("user", signUpData)
        .then((res) => {
          setError({});
          setError2({});
          toast.success(res.data.message);
          setCenteredModal(!centeredModal);
        })
        .catch((err) => {
          toast.error(err.message);
        });
    }
  };

  const validateForm1 = () => {
    let errors = {};
    let formIsValid = true;

    setError2(errors);
    return formIsValid;
  };

  const handleTeam = () => {
    if (validateForm1()) {
      setError2({});
      setFormModal(!formModal);
      setCenteredModal1(!centeredModal1);
    }
  };

  const handleNumber = (phone, e) => {
    let phone1 = phone;
    let countryCode = e.dialCode;
    let phoneNumber = phone1.split(countryCode)[1];
    setsignUpData({
      ...signUpData,
      countryCode,
      phoneNumber,
    });
  };

  const otpVarification = async () => {
    if (otp === "") {
      setotpValidation("OTP is Requried");
    } else {
      setotpValidation("");
      await ApiPostNoAuth("user/otp_verification", { otp: otp })
        .then((res) => {
          setCenteredModal(!centeredModal);

          // settoken(res.data.data);
          // setchangemodal("3");
          history.push("/apps/newIdea");
          toast.success(res.data.message);
        })
        .catch((err) => {
          toast.error(err.message);
        });
    }
    //
  };

  const resend = async () => {
    await ApiPostNoAuth("user/forgot_password", { email: signUpData?.email })
      .then((res) => {
        // setchangemodal("2");
        toast.success(res.data.message);
      })
      .catch((err) => {
        toast.error(err?.message);
      });
  };
  // };

  return (
    <div className="auth-wrapper auth-v2">
      <ToastContainer />
      <Row className="auth-inner m-0">
        <Link className="brand-logo" to="/" onClick={(e) => e.preventDefault()}>
          <img src={logo} alt="pip" className="img-fluid" width={"250px"} />
        </Link>
        <Col className="d-none d-lg-flex align-items-center p-5" lg="8" sm="12">
          <div className="w-100 d-lg-flex align-items-center justify-content-center px-5">
            <img className="img-fluid" src={source} alt="Login V2" />
          </div>
        </Col>
        <Col
          className="d-flex align-items-center auth-bg px-2 p-lg-5"
          lg="4"
          sm="12"
        >
          <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
            <CardTitle tag="h2" className="font-weight-bold mb-1">
              Welcome!
            </CardTitle>
            <CardText className="mb-2">Signup and use PIP for Free</CardText>

            <Form className="auth-register-form mt-2">
              <FormGroup>
                <Label className="form-label" for="register-email">
                  Email
                </Label>
                <Input
                  type="email"
                  value={signUpData.email}
                  id="register-email"
                  name="email"
                  onChange={handleChange}
                  placeholder="john@example.com"
                  // className={classnames({ 'is-invalid': errors['register-email'] })}
                  // innerRef={register({ required: true, validate: value => value !== '' })}
                />

                <div className="validation">{errors?.email}</div>
              </FormGroup>
              <FormGroup>
                <Label className="form-label">Password</Label>
                <InputPasswordToggle
                  autoComplete="new-password"
                  value={signUpData.password}
                  // id='password'
                  name="password"
                  className="input-group-merge"
                  onChange={handleChange}
                  // className={classnames({ 'is-invalid': errors['register-password'] })}
                  // innerRef={register({ required: true, validate: value => value !== '' })}
                />
                <div className="validation">{errors?.password}</div>
              </FormGroup>
              <FormGroup>
                <Label className="form-label">Username</Label>
                <Input
                  type="text"
                  autoComplete="nope"
                  value={signUpData.username}
                  placeholder="johndoe"
                  // id='username'
                  name="username"
                  onChange={handleChange}
                  // className={classnames({ 'is-invalid': errors['register-username'] })}
                  // innerRef={register({ required: true, validate: value => value !== '' })}
                />
                <div className="validation">{errors?.username}</div>
              </FormGroup>

              {/* <FormGroup>
                <CustomInput
                  type='checkbox'
                  id='terms'
                  name='terms'
                  value='terms'
                  label={<Terms />}
                  className='custom-control-Primary'
                  innerRef={register({ required: true })}
                  onChange={e => setTerms(e.target.checked)}
                  invalid={errors.terms && true}
                />
              </FormGroup> */}
              <FormGroup>
                <div className="d-flex justify-content-between">
                  <Label className="form-label" for="register-username">
                    Company Name
                  </Label>
                  <div
                    onClick={() => setFormModal(!formModal)}
                    style={{ color: "#7367f0", cursor: "pointer" }}
                  >
                    <small>Add Company</small>
                  </div>
                </div>
                <Input
                  type="text"
                  value={signUpData.companyName}
                  placeholder="My Company"
                  id="company"
                  name="company"
                  disabled
                  // onChange={handleUsernameChange}
                  // className={classnames({ 'is-invalid': errors['register-username'] })}
                  // innerRef={register({ required: true, validate: value => value !== '' })}
                />
                <div className="validation">
                  {!signUpData.companyName && errors?.companyName}
                </div>
              </FormGroup>
              <FormGroup>
                <Label className="form-label" for="register-username">
                  Country
                </Label>
                <Select
                  theme={selectThemeColors}
                  className="react-select"
                  classNamePrefix="select"
                  onChange={handleCountry}
                  // defaultValue={country}
                  name="countryName"
                  options={colourOptions}
                  isClearable
                />
                {/* {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null} */}
                <div className="validation">{errors?.country}</div>
              </FormGroup>

              <FormGroup>
                <Label className="form-label" for="register-username">
                  Mobile Number for 2FA
                </Label>
                <PhoneInput
                  inputStyle={{ width: "100%", height: "38px" }}
                  country={"us"}
                  // specialLabel={''}
                  // autoFormat={true}
                  value=""
                  onChange={(phone, e) => handleNumber(phone, e)}
                />
                <div className="validation">{errors?.phoneNumber}</div>
                {/* <div className='d-flex outer'>
              <Select
              theme={selectThemeColors}
              className='react-select'
              classNamePrefix='select'
              defaultValue={colourOptions[1]}
              name='clear'
              options={colourOptions}
            />
             <Input
                  autoFocus
                  type='number'
                  value={username}
                  placeholder='800-555-1212'
                  id='2fa'
                  name='register-company'
                  onChange={handleUsernameChange}
                  className={classnames({ 'is-invalid': errors['register-username'] })}
                  style={{width : "80%", marginLeft : "20px"}}
              
                  innerRef={register({ required: true, validate: value => value !== '' })}
                />
              </div> */}
                {/* {Object.keys(valErrors).length && valErrors.username ? (
                  <small className='text-danger'>{valErrors.username}</small>
                ) : null} */}
              </FormGroup>

              <Button.Ripple
                type="button"
                block
                color="primary"
                className="common_button"
                onClick={handleSubmit}
              >
                Sign up
              </Button.Ripple>
            </Form>
            <p className="text-center mt-2">
              <span className="mr-25">Already have an account?</span>
              <Link to="/login">
                <span>Sign in</span>
              </Link>
            </p>
            {/* <div className='divider my-2'>
              <div className='divider-text'>or</div>
            </div>
            <div className='auth-footer-btn d-flex justify-content-center'>
              <Button.Ripple color='facebook'>
                <Facebook size={14} />
              </Button.Ripple>
              <Button.Ripple color='twitter'>
                <Twitter size={14} />
              </Button.Ripple>
              <Button.Ripple color='google'>
                <Mail size={14} />
              </Button.Ripple>
              <Button.Ripple className='mr-0' color='github'>
                <GitHub size={14} />
              </Button.Ripple>
            </div> */}
          </Col>
        </Col>
      </Row>
      <Modal
        isOpen={formModal}
        toggle={() => setFormModal(!formModal)}
        className="modal-dialog-centered"
      >
        <ModalHeader toggle={() => setFormModal(!formModal)}>
          Company Settings
        </ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label for="companyName">Company Name</Label>
            <Input
              type="companyName"
              id="companyName"
              placeholder="Company Name"
              name="companyName"
              value={signUpData.companyName}
              onChange={handleChange}
            />
            <div className="validation">{errors2?.companyName}</div>
          </FormGroup>
          <FormGroup>
            <Label for="teamname">Team Name</Label>
            <Input
              type="teamname"
              id="teamname"
              placeholder="Team Name"
              name="teamName"
              value={signUpData.teamName}
              onChange={handleChange}
            />
            <div className="validation">{errors2?.teamName}</div>
          </FormGroup>

          <div className="d-flex justify-content-between align-items-center ">
            <CardText className="mb-2 mt-2 cursor-pointer">
              Invite Others to Join Team{" "}
            </CardText>
            <div
              style={{
                padding: "6px",
                border: "1px solid #d8d6de",
                borderRadius: "0.357rem",
                cursor: "pointer",
              }}
            >
              <Share2 size={19} />
            </div>
          </div>

          <CardText>
            <b>Note:</b> The person who creates the company controls all the
            teams under the company. The person who creates or names it and
            invites teammates to join controls that team only.
          </CardText>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => {
              handleTeam();
            }}
            className="common_button"
          >
            Create Team
          </Button>{" "}
        </ModalFooter>
      </Modal>

      <Modal
        isOpen={centeredModal}
        toggle={() => setCenteredModal(!centeredModal)}
        className="modal-dialog-centered"
      >
        <ModalHeader toggle={() => setCenteredModal(!centeredModal)}>
          {/* Congratulations! */}
          OTP Verification
        </ModalHeader>
        <ModalBody>
          {/* Please check your email to confirm your email address. Then be sure
          and whitelist Pip.io to receive all important emails about your new
          ideas. Recommended: enable 2FA with your cell phone for security */}
          <OtpInput
            value={otp}
            onChange={(e) => setotp(e)}
            numInputs={6}
            separator={<span> </span>}
            isInputNum
            name="otp"
            // {...formik.getFieldProps("otp")}
            //   inputStyle="w-4"
            inputStyle={otpInput}
          />
          {otpValidation && <div className="validation">{otpValidation}</div>}
          <p className="mt-2 text-right cursor-pointer" onClick={resend}>
            Resend OTP
          </p>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => otpVarification()}
            className="common_button"
          >
            Submit
          </Button>{" "}
        </ModalFooter>
      </Modal>

      <Modal
        isOpen={centeredModal1}
        toggle={() => setCenteredModal1(!centeredModal1)}
        className="modal-dialog-centered"
      >
        <ModalHeader toggle={() => setCenteredModal1(!centeredModal1)}>
          Team Created
        </ModalHeader>
        <ModalBody className="text-center">
          <img src={right} alt="right" className="my-3" />
          <p>You have successfully created a team.</p>
        </ModalBody>
        <ModalFooter>
          <Button
            color="primary"
            onClick={() => setCenteredModal1(!centeredModal1)}
            className="common_button"
          >
            Done
          </Button>{" "}
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default Register;
