import { useState, useContext, Fragment } from 'react'
import classnames from 'classnames'
import Avatar from '@components/avatar'
import { useSkin } from '@hooks/useSkin'
import useJwt from '@src/auth/jwt/useJwt'
import { useDispatch } from 'react-redux'
import { useForm } from 'react-hook-form'
import { toast, ToastContainer, Slide } from 'react-toastify'
import { handleLogin } from '@store/actions/auth'
import { AbilityContext } from '@src/utility/context/Can'
import { Link, useHistory } from 'react-router-dom'
import InputPasswordToggle from '@components/input-password-toggle'
import { getHomeRouteForLoggedInUser, isObjEmpty } from '@utils'
import { Facebook, Twitter, Mail, GitHub, HelpCircle, Coffee } from 'react-feather'
import validator from "validator";

import logo from "../../../assets/images/myImg/PiPlogo.png"
import {
  Alert,
  Row,
  Col,
  CardTitle,
  CardText,
  Form,
  Input,
  FormGroup,
  Label,
  CustomInput,
  Button,
  UncontrolledTooltip
} from 'reactstrap'
import { ApiPost, ApiPostNoAuth, Bucket, ApiGetNoAuth } from "../../../helpers/API/ApiData";

import '@styles/base/pages/page-auth.scss'

const ToastContent = ({ name, role, message }) => (
  <Fragment>
    <div className='toastify-header'>
      <div className='title-wrapper'>
        <Avatar size='sm' color='success' icon={<Coffee size={15} />} />
        <h3 className='toast-title font-weight-bold'>{message}</h3>
      </div>
    </div>
    {/* <div className='toastify-body'>
      <span>{message}</span>
    </div> */}
  </Fragment>
)

const Login = props => {
  const [skin, setSkin] = useSkin()

  const history = useHistory()
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [errors, setError] = useState({});


  // const { register, errors, handleSubmit } = useForm()
  const illustration = skin === 'dark' ? 'login-v2-dark.svg' : 'login-v2.svg',
    source = require(`@src/assets/images/pages/${illustration}`).default

  const validateForm = () => {
    let errors = {};
    let formIsValid = true;

    if (!email) {
      formIsValid = false;
      errors["email"] = "Email is requried";
    }
    if (!validator.isEmail(email)) {
      formIsValid = false;
      errors["email"] = "Enter Valid Email";
    }
    if (!password) {
      formIsValid = false;
      errors["password"] = "Password is Requried";
    }

    setError(errors);
    return formIsValid;
  };

  const handleSubmit = async () => {
    if (validateForm()) {
      await ApiPostNoAuth(process.env.REACT_APP_LOGIN, { email, password })
        .then((res) => {
          const data = res.data.data
          data.ability = [{ action: "manage", subject: "all" }]
          data.role = "admin"
          localStorage.setItem("userData", JSON.stringify(data))
          localStorage.setItem("accessToken", JSON.stringify(res.data.data.token))
          localStorage.setItem("refreshToken", JSON.stringify(res.data.data.refresh_token))
          toast.success(res.data.message)
          setError({})

          // let a = localStorage.getItem("path")
          // history.push("/")
          setTimeout(() => {
            window.location.pathname = "/"
          }, 2000);

          // toast.success( res.data.message, "uyuwewriewyr")
        })
        .catch((err) => {
          toast.error(err.message)
        });
    }
  }

  return (
    <div className='auth-wrapper auth-v2'>
      <ToastContainer />
      <Row className='auth-inner m-0'>
        <Link className='brand-logo' to='/' onClick={e => e.preventDefault()}>

          <img src={logo} alt="pip" width={"250px"} />
        </Link>
        <Col className='d-none d-lg-flex align-items-center p-5' lg='8' sm='12'>
          <div className='w-100 d-lg-flex align-items-center justify-content-center px-5'>
            <img className='img-fluid' src={source} alt='Login V2' />
          </div>
        </Col>
        <Col className='d-flex align-items-center auth-bg px-2 p-lg-5' lg='4' sm='12'>
          <Col className='px-xl-2 mx-auto' sm='8' md='6' lg='12'>
            <CardTitle tag='h2' className='font-weight-bold mb-1'>
              Welcome!
            </CardTitle>
            <CardText className='mb-2'>The Profitable Idea Platform, Your Ideas, Your Fortune</CardText>

            <Form className='auth-login-form mt-2' >
              <FormGroup>
                <Label className='form-label' >
                  Email
                </Label>
                <Input
                  autoComplete="nope"
                  type='email'
                  value={email}
                  name='email'
                  placeholder='john@example.com'
                  onChange={e => setEmail(e.target.value)}
                // className={errors?.email && "is-invalid"}
                // className={classnames({ 'is-invalid': errors['login-email'] })}
                // innerRef={register({ required: true, validate: value => value !== '' })}
                />
                <div className='validation'>
                  {errors?.email}
                </div>
                {/* {errors?.email &&   toast.error(errors?.email) } */}
              </FormGroup>
              <FormGroup>
                <div className='d-flex justify-content-between'>
                  <Label className='form-label' >
                    Password
                  </Label>
                  <Link to='/forgot-password'>
                    <small>Forgot Password?</small>
                  </Link>
                </div>
                <InputPasswordToggle
                  autoComplete="new-password"
                  value={password}
                  name='password'
                  className='input-group-merge'
                  onChange={e => setPassword(e.target.value)}
                // className={errors?.password && "is-invalid"}
                // className={classnames({ 'is-invalid': errors['login-password'] })}
                // innerRef={register({ required: true, validate: value => value !== '' })}
                />
                <div className='validation'>
                  {errors?.password}
                </div>
              </FormGroup>
              {/* <FormGroup>
                <CustomInput type='checkbox' className='custom-control-Primary' id='remember-me' label='Remember Me' />
              </FormGroup> */}
              <Button.Ripple type='button' color='primary' className='common_button' block onClick={handleSubmit}>
                Sign in
              </Button.Ripple>
            </Form>
            <p className='text-center mt-2'>
              <span className='mr-25'>Don’t have an account ? </span>
              <Link to='/register'>
                <span>Sign up</span>
              </Link>
            </p>
            <div className='text-center'>

              <p>Copyright Vertu Medical Technologies, LLC. All rights reserved. 2021</p>
            </div>
            {/* <div className='divider my-2'>
              <div className='divider-text'>or</div>
            </div>
            <div className='auth-footer-btn d-flex justify-content-center'>
              <Button.Ripple color='facebook'>
                <Facebook size={14} />
              </Button.Ripple>
              <Button.Ripple color='twitter'>
                <Twitter size={14} />
              </Button.Ripple>
              <Button.Ripple color='google'>
                <Mail size={14} />
              </Button.Ripple>
              <Button.Ripple className='mr-0' color='github'>
                <GitHub size={14} />
              </Button.Ripple>
            </div> */}
          </Col>
        </Col>
      </Row>
    </div>
  )
}

export default Login
