import { isUserLoggedIn } from "@utils";
import { useSkin } from "@hooks/useSkin";
import { ChevronLeft } from "react-feather";
import { Link, Redirect, useHistory } from "react-router-dom";
import {
  Row,
  Col,
  CardTitle,
  CardText,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";
import "@styles/base/pages/page-auth.scss";
import logo from "../../../assets/images/myImg/PiPlogo.png";
import { useState } from "react";
import OtpInput from "react-otp-input";
import InputPasswordToggle from "@components/input-password-toggle";
import OtpTimer from "otp-timer";
import validator from "validator";
import {
  ApiPost,
  ApiPostNoAuth,
  Bucket,
  ApiGetNoAuth,
} from "../../../helpers/API/ApiData";
import { toast, ToastContainer, Slide } from "react-toastify";

const ForgotPassword = () => {
  const [skin, setSkin] = useSkin();
  const [changemodal, setchangemodal] = useState("1");
  const [npass, setnpass] = useState("");
  const [npassValidation, setnpassValidation] = useState("");
  const [cpass, setcpass] = useState("");
  const [cpassValidation, setcpassValidation] = useState("");
  const [email, setemail] = useState("");
  const [emailValidation, setemailValidation] = useState("");
  const [otp, setotp] = useState("");
  const [otpValidation, setotpValidation] = useState("");
  const [token, settoken] = useState({});

  const history = useHistory();

  const submitModal1 = async () => {
    if (email === "") {
      setemailValidation("Email is Requried");
    } else if (!validator.isEmail(email)) {
      setemailValidation("Enter Valid Email");
    } else {
      setemailValidation("");
      await ApiPostNoAuth("user/forgot_password", { email: email })
        .then((res) => {
          setchangemodal("2");
          toast.success(res.data.message);
        })
        .catch((err) => {
          toast.error(err?.message);
        });
    }
  };

  const submitModal2 = async () => {
    if (otp === "") {
      setotpValidation("OTP is Requried");
    } else {
      setotpValidation("");
      await ApiPostNoAuth("user/otp_verification", { otp: otp })
        .then((res) => {
          settoken(res.data.data);
          setchangemodal("3");
          toast.success(res.data.message);
        })
        .catch((err) => {
          toast.error(err.message);
        });
    }
  };

  const submitModal3 = async () => {
    if (npass === "") {
      setnpassValidation("New Password is Requried");
    } else {
      setnpassValidation("");
    }
    if (cpass === "") {
      setcpassValidation("Confirm Password is Requried");
    } else {
      setcpassValidation("");
    }
    if (npass !== cpass) {
      setcpassValidation("Passwors is Not Match");
    } else {
      setcpassValidation("");
    }
    if (npass !== "" && cpass !== "" && npass === cpass) {
      await ApiPostNoAuth("user/reset_password", {
        id: token._id,
        authToken: token.authToken,
        password: npass,
      })
        .then((res) => {
          toast.success(res.data.message);
          history.push("/");
        })
        .catch((err) => {
          toast.error(err.message);
        });
    }
  };

  const illustration =
      skin === "dark"
        ? "forgot-password-v2-dark.svg"
        : "forgot-password-v2.svg",
    source = require(`@src/assets/images/pages/${illustration}`).default;

  if (!isUserLoggedIn()) {
    return (
      <div className="auth-wrapper auth-v2">
        <ToastContainer />
        <Row className="auth-inner m-0">
          <Link
            className="brand-logo"
            to="/"
            onClick={(e) => e.preventDefault()}
          >
            <img src={logo} alt="pip" width={"250px"} />
          </Link>
          <Col
            className="d-none d-lg-flex align-items-center p-5"
            lg="8"
            sm="12"
          >
            <div className="w-100 d-lg-flex align-items-center justify-content-center px-5">
              <img className="img-fluid" src={source} alt="Login V2" />
            </div>
          </Col>
          {changemodal === "1" && (
            <Col
              className="d-flex align-items-center auth-bg px-2 p-lg-5"
              lg="4"
              sm="12"
            >
              <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
                <CardTitle tag="h2" className="font-weight-bold mb-1">
                  Forgot Password
                </CardTitle>
                <CardText className="mb-2">
                  Enter your email to get the otp
                </CardText>
                <Form
                  className="auth-forgot-password-form mt-2"
                  onSubmit={(e) => e.preventDefault()}
                >
                  <FormGroup>
                    <Label className="form-label" for="login-email">
                      Email
                    </Label>
                    <Input
                      type="email"
                      id="login-email"
                      placeholder="john@example.com"
                      autoFocus
                      value={email}
                      onChange={(e) => setemail(e.target.value)}
                    />
                    {emailValidation && (
                      <div className="validation">{emailValidation}</div>
                    )}
                  </FormGroup>
                  <Button.Ripple
                    color="primary"
                    block
                    onClick={() => submitModal1()}
                    className="common_button"
                  >
                    Submit
                  </Button.Ripple>
                </Form>
                <p className="text-center mt-2">
                  <Link to="/login">
                    <ChevronLeft className="mr-25" size={14} />
                    <span className="align-middle">Back to login</span>
                  </Link>
                </p>
              </Col>
            </Col>
          )}
          {changemodal === "2" && (
            <Col
              className="d-flex align-items-center auth-bg px-2 p-lg-5"
              lg="4"
              sm="12"
            >
              <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
                <CardTitle tag="h2" className="font-weight-bold mb-1">
                  Verification
                </CardTitle>
                <CardText className="mb-2">
                  Enter the otp sent to your given email 01:59
                </CardText>
                <Form
                  className="auth-forgot-password-form mt-2"
                  onSubmit={(e) => e.preventDefault()}
                >
                  <FormGroup>
                    <Label className="form-label" for="login-email">
                      OTP
                    </Label>
                    <div className="d-flex ">
                      <OtpInput
                        value={otp}
                        onChange={(e) => setotp(e)}
                        numInputs={6}
                        separator={<span className="mr-10"> </span>}
                        isInputNum
                        inputStyle={{
                          width: "45px",
                          height: "45px",
                          marginRight: "7px",
                        }}
                        containerStyle={{ width: "100%" }}
                      />
                      <br />
                      <br />
                    </div>
                    {otpValidation && (
                      <div className="validation">{otpValidation}</div>
                    )}
                  </FormGroup>
                  <Button.Ripple
                    color="primary"
                    block
                    onClick={() => submitModal2()}
                    className="common_button"
                  >
                    Submit
                  </Button.Ripple>
                </Form>
                <p className="text-center mt-2 d-flex justify-content-between">
                  <Link to="/login">
                    <ChevronLeft className="mr-25" size={14} />
                    <span className="align-middle">Back to login</span>
                  </Link>
                  <div className="otp">
                    <OtpTimer
                      seconds={59}
                      minutes={1}
                      resend={() => {
                        submitModal1();
                      }}
                      text="Resend OTP in"
                      ButtonText="Resend OTP"
                      textColor="#7367f0"
                      buttonColor={"#7367f0"}
                      background={"#fff"}
                    />
                  </div>
                  {/* <Link to=''>
                    <small>Resend OTP</small>
                  </Link> */}
                </p>
              </Col>
            </Col>
          )}
          {changemodal === "3" && (
            <Col
              className="d-flex align-items-center auth-bg px-2 p-lg-5"
              lg="4"
              sm="12"
            >
              <Col className="px-xl-2 mx-auto" sm="8" md="6" lg="12">
                <CardTitle tag="h2" className="font-weight-bold mb-1">
                  Forgot Password? 🔒
                </CardTitle>
                <CardText className="mb-2">
                  Enter your email and we'll send you instructions to reset your
                  password
                </CardText>
                <Form
                  className="auth-forgot-password-form mt-2"
                  onSubmit={(e) => e.preventDefault()}
                >
                  <FormGroup>
                    <div className="d-flex justify-content-between">
                      <Label className="form-label" for="login-password">
                        New Password
                      </Label>
                    </div>
                    <InputPasswordToggle
                      value={npass}
                      id="login-password"
                      name="login-password"
                      className="input-group-merge"
                      placeholder="123"
                      onChange={(e) => setnpass(e.target.value)}
                      // className={classnames({ 'is-invalid': errors['login-password'] })}
                      // innerRef={register({ required: true, validate: value => value !== '' })}
                    />
                    {npassValidation && (
                      <div className="validation">{npassValidation}</div>
                    )}
                  </FormGroup>
                  <FormGroup>
                    <div className="d-flex justify-content-between">
                      <Label className="form-label" for="login-password">
                        Confirm Password
                      </Label>
                    </div>
                    <InputPasswordToggle
                      value={cpass}
                      placeholder="123"
                      id="login-password"
                      name="login-password"
                      className="input-group-merge"
                      onChange={(e) => setcpass(e.target.value)}
                      // className={classnames({ 'is-invalid': errors['login-password'] })}
                      // innerRef={register({ required: true, validate: value => value !== '' })}
                    />
                    {cpassValidation && (
                      <div className="validation">{cpassValidation}</div>
                    )}
                  </FormGroup>
                  <Button.Ripple
                    color="primary"
                    block
                    onClick={() => submitModal3()}
                    className="common_button"
                  >
                    Update Password
                  </Button.Ripple>
                </Form>
                <p className="text-center mt-2">
                  <Link to="/login">
                    <ChevronLeft className="mr-25" size={14} />
                    <span className="align-middle">Back to login</span>
                  </Link>
                </p>
              </Col>
            </Col>
          )}
        </Row>
      </div>
    );
  } else {
    return <Redirect to="/" />;
  }
};

export default ForgotPassword;
